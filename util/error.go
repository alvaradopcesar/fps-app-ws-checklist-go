package util

import "log"

// CheckErr ..mensaje de error
func CheckErr(err error, msg string) {
	if err != nil {
		log.Fatalln(msg, err)
	}
}
