package controller

import (
	"fps-app-ws-checklist-go/pkg/model"
	"fps-app-ws-checklist-go/pkg/service"
	"github.com/labstack/echo"
)

// PostRespuestaChecklist obtiene todos los usuarios
func PostRespuestaChecklist(c echo.Context) error {
	var checklist model.Checklist
	if err := c.Bind(&checklist); err != nil {
		return err
	}
	AndroidVersion := c.Request().Header.Get("X-Version-App")
	authorization := c.Request().Header.Get("Authorization")
	if authorization == "" {
		return c.JSON(400, "Debe Ingresar Authorization !!")
	}
	CodUsu, err := service.GetCodUSerFromToken(authorization)
	if err != nil {
		return c.JSON(400, "No se puede obtener usuario!!")
	}
	return c.JSON(200, service.RespuestaChecklistSave(CodUsu,AndroidVersion,checklist))
}

func PostRespuestaChecklistMovil(c echo.Context) error {
	var checklistMobilRequest service.ChecklistMobilRequest
	if err := c.Bind(&checklistMobilRequest); err != nil {
		return err
	}
	AndroidVersion := c.Request().Header.Get("X-Version-App")
	authorization := c.Request().Header.Get("Authorization")
	if authorization == "" {
		return c.JSON(400, "Debe Ingresar Authorization !!")
	}
	CodUsu, err := service.GetCodUSerFromToken(authorization)
	if err != nil {
		return c.JSON(400, "No se puede obtener usuario!!")
	}
	return c.JSON(200,service.RespuestaChecklistSaveMobil(CodUsu,AndroidVersion,checklistMobilRequest))
}

