package controller

import (
	"fps-app-ws-checklist-go/pkg/model"
	nivelService "fps-app-ws-checklist-go/pkg/service"
	"github.com/labstack/echo"
	"strconv"
)

// GetNivel obtiene todos los usuarios
func GetNivel(c echo.Context) error {
	return c.JSON(200, nivelService.NivelFindAll())
}

func PostNivel(c echo.Context) error {
	var nivel model.Nivel
	if err := c.Bind(&nivel); err != nil {
		return err
	}
	return c.JSON(200, nivelService.NivelSave(nivel))
}

func GetNivelUsuarioLocaByNivelID(c echo.Context) error {
	nivelID, _ := strconv.ParseInt(c.Param("nivelID"), 10, 32)
	return c.JSON(200, nivelService.NivelUsuarioLocaByNivelID(nivelID))
}