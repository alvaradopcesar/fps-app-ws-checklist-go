package response

import (
	"fps-app-ws-checklist-go/pkg/model"
)

// Maestros tabla usuaruios
type Maestros struct {
	Locales  []model.PBLLocal        `json:"locales"`
	Tecnicos []model.PBLUsuarioLocal `json:"tecnicos"`
	QFS      []model.PBLUsuarioLocal `json:"qfs"`
}
