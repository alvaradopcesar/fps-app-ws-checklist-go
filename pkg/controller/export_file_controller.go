package controller

import (
	"fps-app-ws-checklist-go/pkg/service"
	"github.com/labstack/echo"
	"time"
)

// GetChecklistExportTemplate01 obtiene archivo segun fechas
func GetChecklistExportTemplate01(c echo.Context) error {
	fecini, err := time.Parse("2006-01-02", c.Param("fecini")[0:10])
	if err != nil {
		return err
	}
	fecfin, err := time.Parse("2006-01-02", c.Param("fecfin")[0:10])
	if err != nil {
		return err
	}
	fileName ,finalFileName, err := service.ExportFileTemplate01(fecini,fecfin)
	if err != nil {
		return c.JSON(500, err )
	}
	return c.Attachment(fileName, finalFileName)
}

