package controller

import (
	"fmt"
	"fps-app-ws-checklist-go/pkg/model"
	"fps-app-ws-checklist-go/pkg/service"
	"strconv"

	"github.com/labstack/echo"
)

// GetChecklist obtiene todos los usuarios
func GetChecklist(c echo.Context) error {
	return c.JSON(200, service.ChecklistFindAll())
}

// GetChecklistByID busqueda por codigo
func GetChecklistByID(c echo.Context) error {
	i64, err := strconv.ParseInt(c.Param("id"), 10, 32)
	checklist, err := service.GetChecklistByID(i64)
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON(200, checklist)
}

// PostChecklist grabar checklist
func PostChecklist(c echo.Context) error {
	var checklist model.Checklist
	if err := c.Bind(&checklist); err != nil {
		return err
	}
	return c.JSON(200, service.ChecklistSave(checklist))
}

func GetChecklistUserList(c echo.Context) error {
	i64, err := strconv.ParseInt(c.Param("estado"), 10, 32)
	estado := int32(i64)
	codUsu := c.Param("codUsu")
	checklistUserList , err := service.ChecklistUserList(estado,codUsu)
	if err != nil {
		fmt.Println(err)
		return c.JSON(400, err)
	}
	return c.JSON(200, checklistUserList)
}

func GetChecklistListar(c echo.Context) error {
	authorization := c.Request().Header.Get("Authorization")
	if authorization == "" {
		return c.JSON(400, "Debe Ingresar Authorization !!")
	}
	checlistList , err  := service.GetChecklistListar(authorization)
	if err != nil {
		return err
	}
	return c.JSON(200, checlistList)
}
