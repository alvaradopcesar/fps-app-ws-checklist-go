package controller

import (
	areaService "fps-app-ws-checklist-go/pkg/service"

	"github.com/labstack/echo"
)

// GetArea obtiene todos los usuarios
func GetArea(c echo.Context) error {
	return c.JSON(200, areaService.AreaFindAll())
}
