package controller

import (
	"fps-app-ws-checklist-go/pkg/service"
	"github.com/labstack/echo"
)

// GetArbol obtiene todos los usuarios
func GetArbol(c echo.Context) error {
	return c.JSON(200, service.GetArbolTree(0))
}

//GetLocalesByDNI Obtiene Locales por DNI ..segun arbol
func GetLocalesByDNI(c echo.Context) error {
	authorization := c.Request().Header.Get("Authorization")
	if authorization == "" {
		return c.JSON(400, "Debe Ingresar Authorization !!")
	}
	dni, err := service.GetDniFromToken(authorization)
	if err != nil {
		return c.JSON(400,"Error indentificando el usuario")
	}
	data, _ := service.GetLocalesByDNI(dni)
	return c.JSON(200, data)
}

