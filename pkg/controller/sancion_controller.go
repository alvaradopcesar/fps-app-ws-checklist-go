package controller

import (
	sanciontService "fps-app-ws-checklist-go/pkg/service"

	"github.com/labstack/echo"
)

// GetSancion obtiene todos los usuarios
func GetSancion(c echo.Context) error {
	return c.JSON(200, sanciontService.SancionFindAll())
}
