package controller

import (
	"fps-app-ws-checklist-go/pkg/service"
	"github.com/labstack/echo"
	"time"
)

// GetArea obtiene todos los usuarios
func GetListaAsistencia(c echo.Context) error {
	return c.JSON(200, service.AsistenciaList(c.Param("DNI")))
}

func GetAsistenciaByFechas(c echo.Context) error {
	fecini, err := time.Parse("2006-01-02", c.Param("fecini")[0:10])
	if err != nil {
		return err
	}
	fecfin, err := time.Parse("2006-01-02", c.Param("fecfin")[0:10])
	if err != nil {
		return err
	}
	fileName ,finalFileName, err := service.ExportFileTemplate02(fecini,fecfin)
	if err != nil {
		return c.JSON(500, err )
	}
	return c.Attachment(fileName, finalFileName)
}

func PostAsistenciaMarcacion(c echo.Context) error {
	var asistenciaMarcacionRequest service.AsistenciaMarcacionRequest
	if err := c.Bind(&asistenciaMarcacionRequest); err != nil {
		return err
	}
	authorization := c.Request().Header.Get("Authorization")
	if authorization == "" {
		return c.JSON(400, "Debe Ingresar Authorization !!")
	}
	dni, err := service.GetDniFromToken(authorization)
	if err != nil {
		return c.JSON(400,"Error indentificando el usuario")
	}
	return c.JSON(200,service.AsistenciaMarcacion(dni, asistenciaMarcacionRequest))
}

