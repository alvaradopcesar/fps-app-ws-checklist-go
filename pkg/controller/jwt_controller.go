package controller

import (
	"fps-app-ws-checklist-go/pkg/service"
	"github.com/labstack/echo"
)

// Login
func LoginWeb(c echo.Context) error {
	return service.Login(c, false)
}

func LoginMobile(c echo.Context) error {
	return service.Login(c, true)
}