package controller

import (
	"fps-app-ws-checklist-go/pkg/controller/response"
	"fps-app-ws-checklist-go/pkg/model"
	"fps-app-ws-checklist-go/pkg/repository"
	"github.com/labstack/echo"
)

// GetMaestros obtiene todos los usuarios
func GetMaestros(c echo.Context) error {
	var maestro response.Maestros
	maestro.Locales = []model.PBLLocal{}
	maestro.Tecnicos = repository.PBLUsuarioLocalFindbyCargo("TECNICOS")
	maestro.QFS = repository.PBLUsuarioLocalFindbyCargo("QUIMICOS")
	return c.JSON(200, maestro)
}
