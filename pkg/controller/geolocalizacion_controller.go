package controller

import (
	"fps-app-ws-checklist-go/pkg/model"
	"fps-app-ws-checklist-go/pkg/service"
	"github.com/labstack/echo"
	"strconv"
)

// GetChecklistByID busqueda por codigo
func GetLocalByID(c echo.Context) error {
	latitud, _ := strconv.ParseFloat(c.Param("latitud"), 64)
	longitudC := c.Param("longitud")
	longitud, _ := strconv.ParseFloat(longitudC, 64)
	arbolList,err := service.GetLocalByPerson(service.LocalByPersonRequest{DNI:c.Param("DNI"), Latitud:latitud, Longitud:longitud})
	if err != nil {
		return c.JSON(400, err)
	}
	message := ""
	if len(arbolList) == 0 {
		message = "No tiene ningun local cerca"
	}
	type Response struct{
		Data  []model.ArbolGeoReturn `json:"data"`
		Message string `json:"message"`
	}
	return c.JSON(200, Response{ Data: arbolList, Message: message})

}
