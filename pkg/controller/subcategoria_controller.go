package controller

import (
	subCategoriaService "fps-app-ws-checklist-go/pkg/service"

	"github.com/labstack/echo"
)

// GetSubCategoria obtiene todos los usuarios
func GetSubCategoria(c echo.Context) error {
	return c.JSON(200, subCategoriaService.SubCategoriaFindAll())
}
