package controller

import (
	"fps-app-ws-checklist-go/pkg/model"
	"fps-app-ws-checklist-go/pkg/service"
	"github.com/labstack/echo"
	"log"
	"strconv"
)

func PostUploadFile(c echo.Context) error {
	log.Println("PostUploadFile ini ")
	uploadFile,upLoadFileResponse, err := getParamsUploadFile(c)
	if err != nil {
		return c.JSON(400, upLoadFileResponse)
	}
	log.Println("PostUploadFile fin ")
	return c.JSON(200, service.UploadFile(uploadFile))
}

func PostExistePhoto(c echo.Context) error {
	uploadFile := model.UploadFileRequest{DNI: c.FormValue("dni")}
	return c.JSON(200, service.ExistePhoto(uploadFile))
}

func PostExisteDNI(c echo.Context) error {
	uploadFile := model.UploadFileRequest{DNI: c.FormValue("dni")}
	return c.JSON(200, service.ExisteDNI(uploadFile))
}

func getParamsUploadFile(c echo.Context) (model.UploadFileRequest,model.UploadFileResponse,error ){
	var uploadFile model.UploadFileRequest
	var upLoadFileResponse model.UploadFileResponse
	i, err := strconv.ParseInt(c.FormValue("status"), 10, 32)
	if err != nil {
		upLoadFileResponse.Status = false
		upLoadFileResponse.Message = err.Error()
		return uploadFile,upLoadFileResponse,err
	}
	uploadFile.Status = int(i)
	uploadFile.UserCode = c.FormValue("userCode")
	uploadFile.DNI = c.FormValue("dni")
	uploadFile.FilePhoto , err = c.FormFile("file")
	return uploadFile,upLoadFileResponse,nil
}

