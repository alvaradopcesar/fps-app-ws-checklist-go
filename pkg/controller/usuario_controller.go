package controller

import (
	"fps-app-ws-checklist-go/pkg/service"

	"github.com/labstack/echo"
)

func GetUsuarioLocalList(c echo.Context) error {
	nombreToFind := c.Param("nombreToFind")
	data ,  err := service.UsuarioLocalList(nombreToFind)
	if err != nil {
		return c.JSON(400, err)
	}
	return c.JSON( 200 , data)
}

