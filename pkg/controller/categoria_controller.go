package controller

import (
	categoriaService "fps-app-ws-checklist-go/pkg/service"

	"github.com/labstack/echo"
)

// GetCategoria obtiene todos los usuarios
func GetCategoria(c echo.Context) error {
	return c.JSON(200, categoriaService.CategoriaFindAll())
}
