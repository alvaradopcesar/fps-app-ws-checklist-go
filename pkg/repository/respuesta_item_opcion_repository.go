package repository

import (
	"fps-app-ws-checklist-go/config"
	"fps-app-ws-checklist-go/pkg/model"
)

func RespouestaItemOpcionInsert( respuestaItem model.RespuestaItemOpcion ) (model.RespuestaItemOpcion,error) {
	respuestaItemOpcionResponse := model.RespuestaItemOpcion{}
	queryInsert := `
	INSERT INTO respuestaitem_opcion ( RespuestaItem_id, opcionrespuesta_id )
	VALUES (?, ?)`
	_, err := config.Dbmap.Exec(queryInsert,
		respuestaItem.RespuestaItemId,
		respuestaItem.OpcionrespuestaId,
	)
	if err != nil {
		return respuestaItemOpcionResponse, err
	}
	return respuestaItemOpcionResponse, nil

}