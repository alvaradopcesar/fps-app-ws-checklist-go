package repository

import (
	db "fps-app-ws-checklist-go/config"
	model "fps-app-ws-checklist-go/pkg/model"
)

// PBLUsuarioLocalFindbyCargo a
func PBLUsuarioLocalFindbyCargo(cargo string) []model.PBLUsuarioLocal {
	var localesUsuario []model.PBLUsuarioLocal
	query := `
	select 'INKA' as cod_empresa,cod_local,b.cod_usu,' ' as sec_usu_local,' ' as login_usu,' ' as clave_usu,'A' as est_usu  ,t.cod_cargo 
			from encuestas.local_usuario b, 
			      ptoventa.ce_mae_trab t ,
                  ptoventa.ce_cargo c
			where est_usu = 'a' 
			  and b.cod_usu = t.cod_trab 
			  and t.cod_cargo = c.cod_cargo
              and upper(c.desc_cargo) like '%` + cargo + `%'`
	db.Dbmap.Select(&localesUsuario, query)
	return localesUsuario
}
