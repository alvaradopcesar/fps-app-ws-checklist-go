package repository

import (
	db "fps-app-ws-checklist-go/config"
	model "fps-app-ws-checklist-go/pkg/model"
)

// PblLocalByUser a
func PblLocalByUser(codUsu string) []model.PBLLocal {
	var locales []model.PBLLocal
	// db.Dbmap.Select(&locales, "SELECT cod_local,cod_cia,desc_local,ind_local_prov,direc_local,ubdep,ubdis,ubprv FROM ptoventa.pbl_local where est_local ='A'")
	db.Dbmap.Select(&locales,
		`select a.cod_local,a.cod_cia,a.desc_local,a.ind_local_prov,a.direc_local,a.ubdep,a.ubdis,a.ubprv
  			from ptoventa.pbl_local a,
       	local_usuario l
  	where a.cod_local = l.cod_local
   		 and l.cod_usu like '%`+codUsu+`%'
   		and a.est_local = 'A'`)
	return locales
}
