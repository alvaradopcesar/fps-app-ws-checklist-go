package repository

import (
	db "fps-app-ws-checklist-go/config"
	"fps-app-ws-checklist-go/pkg/model"
	"log"
)

// OpcionRespuestaByChkList estrive one record by cod
func OpcionRespuestaByChkList(codItem int64) []model.OpcionRespuesta {
	var opcionRespuestas []model.OpcionRespuesta
	_, err := db.Dbmap.Select(&opcionRespuestas,
	`Select 
	id,
	connotacion,
	enunciado
	cod_item,
	false as selected
	from opcionrespuesta where cod_item = ?`,
	codItem)
	if err != nil {
		log.Println(err)
	}
	return opcionRespuestas
}


// SaveOpcionRespuesta grabar opcionRespuesta
func SaveOpcionRespueata(opcionRespuesta model.OpcionRespuesta) (model.OpcionRespuesta, error) {
	cant, err := db.Dbmap.SelectInt("select count(*) from opcionrespuesta where id = ?", opcionRespuesta.ID)
	if err != nil {
		return opcionRespuesta, err
	}
	if cant == 0 {
		return insertOpcionRespuesta(opcionRespuesta)
	} else {
		return updateOpcionRespuesta(opcionRespuesta)
	}
}

// insertOpcionRespuesta insert into checklist
func insertOpcionRespuesta(opcionRespuesta model.OpcionRespuesta) (model.OpcionRespuesta, error) {
	queryInsert := `
	INSERT INTO opcionrespuesta (connotacion, enunciado, cod_item)
		VALUES ( ?, ?, ?);`
	_, err := db.Dbmap.Exec(queryInsert,
		opcionRespuesta.Connotacion,
		opcionRespuesta.Enunciado,
		opcionRespuesta.CodItem,
	)
	// 	loan.InitialAmount, loan.AnnualInterestRate, loan.StartDate.Format("2006-01-02T15:04:05.195266-05:00"))
	if err != nil {
		return opcionRespuesta, err
	}
	return opcionRespuesta, nil
}

// updateOpcionRespuesta actualizar OpcionRespuesta
func updateOpcionRespuesta(opcionRespuesta model.OpcionRespuesta) (model.OpcionRespuesta, error) {
	queryUpdate := `
	UPDATE opcionrespuesta 
		SET connotacion = ?,
			enunciado = ?,
			cod_item = ?
		where id = ?`
	_, err := db.Dbmap.Exec(queryUpdate,
		opcionRespuesta.Connotacion,
		opcionRespuesta.Enunciado,
		opcionRespuesta.CodItem,
		opcionRespuesta.ID,
	)
	if err != nil {
		return opcionRespuesta, err
	}
	if err != nil {
		return opcionRespuesta, err
	}
	return opcionRespuesta, nil
}

// OpcionRespuestaByChkList estrive one record by cod
func OpcionRespuestaByChkListId(codChList int64) ([]model.OpcionRespuesta, error ){
	opcionRespuestas := []model.OpcionRespuesta{}
	_, err := db.Dbmap.Select(&opcionRespuestas,`
		select *
			from opcionrespuesta
		where cod_item in ( select cod_item from item where cod_ch_list = ? )`,
		codChList)
	if err != nil {
		log.Println(err)
		return opcionRespuestas,err
	}
	return opcionRespuestas,nil
}


