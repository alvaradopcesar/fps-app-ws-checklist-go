package repository

import (
	db "fps-app-ws-checklist-go/config"
	model "fps-app-ws-checklist-go/pkg/model"
	"log"
)

// ItemByChkList restrive one record by cod
func ChecklistUserByChkList(id int64) []model.ChecklistUsuario {
	var checklistUsuario []model.ChecklistUsuario
	_, err := db.Dbmap.Select(&checklistUsuario, "Select * from checklist_usuario where cod_ch_list = ?", id)
	if err != nil {
		log.Println(err)
	}
	return checklistUsuario
}
