package repository

import (
	db "fps-app-ws-checklist-go/config"
	"fps-app-ws-checklist-go/pkg/model"
	"log"
)

//NivelAllRoot d
func NivelAllRoot() []model.Nivel {
	var nivel []model.Nivel
	_, err := db.Dbmap.Select(&nivel, "Select * from nivel where estado = 1 and id_padre is null")
	if err != nil {
		log.Println(err)
	}
	return nivel
}

// NivelAllByPadre d
func NivelAllByPadre(idPadre int64) []model.Nivel {
	var nivel []model.Nivel
	_, err := db.Dbmap.Select(&nivel, "Select * from nivel where estado = 1 and id_padre = ?", idPadre)
	if err != nil {
		log.Println(err)
	}
	return nivel
}

// NivelSave grabar checklist
func NivelSave(nivel model.Nivel) (model.Nivel, error) {
	cant, err := db.Dbmap.SelectInt("select count(*) from nivel where id = ?", nivel.ID)
	if err != nil {
		return nivel, err
	}
	if cant == 0 {
		return nivelInsert(nivel)
	} else {
		return nivelUpdate(nivel)
	}
}

// insertNivel insert into nivel
func nivelInsert(nivel model.Nivel) (model.Nivel, error) {
	//nivel.FechaCreacion = time.Now()
	queryInsert := `
	INSERT INTO nivel (id_padre,codigo,descripcion01,descripcion02,estado,fechacreacion,usuariocreacion) 
		values ( ?, ?, ?, ?, ?, ?, ? )`
	result, err := db.Dbmap.Exec(queryInsert,
		nivel.IDPadre,
		nivel.Codigo,
		nivel.Descripcion01,
		nivel.Descripcion02,
		nivel.Estado,
		nivel.FechaCreacion,
		nivel.UsuarioCreacion,
	)
	if err != nil {
		return nivel, err
	}
	nivel.ID,_ = result.LastInsertId()
	return nivel, nil
}

// UpdateChecklist actualizar Checklist
func nivelUpdate(nivel model.Nivel) (model.Nivel, error) {
	queryUpdate := `
	UPDATE nivel
		set id_padre = ?, 
			codigo = ?, 
			descripcion01 = ?, 
			descripcion02 = ?, 
			estado = ?
		where id = ?`
	_, err := db.Dbmap.Exec(queryUpdate,
		nivel.IDPadre,
		nivel.Codigo,
		nivel.Descripcion01,
		nivel.Descripcion02,
		nivel.Estado,
		nivel.ID,
	)
	if err != nil {
		return nivel, err
	}
	return nivel, nil
}
