package repository

import (
	"fmt"
	"fps-app-ws-checklist-go/config"
	"fps-app-ws-checklist-go/pkg/model"
)

func RespouestaChecklistInsert( respuestaChecklist model.RespuestaChecklist) (model.RespuestaChecklist,error) {
	respuestaChecklistResponse := model.RespuestaChecklist{}
	queryInsert := `
	INSERT INTO respuestachecklist (aprobacionqf, aprobacionT, aprobacionU, creadoDesde, estado, fechaCreacion, fechaFinalizacion, 
				cod_ch_list, cod_usu_u, fechaguardado, versionApp)
	VALUES (?, ?, ?, 0, ?, sysdate(), sysdate(), ?, ?, sysdate(), ?)`
	result, err := config.Dbmap.Exec(queryInsert,
		respuestaChecklist.AprobacionQF,
		respuestaChecklist.AprobacionT,
		respuestaChecklist.AprobacionU,
		respuestaChecklist.Estado,
		respuestaChecklist.CodChList,
		respuestaChecklist.CodUsu,
		respuestaChecklist.VersionApp,
	)
	if err != nil {
		return respuestaChecklistResponse, err
	}
	id , _ := result.LastInsertId()
	fmt.Println(id)
	respuestaChecklistResponse = respuestaChecklist
	respuestaChecklistResponse.ID = id
	return respuestaChecklistResponse, nil
}