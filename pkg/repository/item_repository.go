package repository

import (
	db "fps-app-ws-checklist-go/config"
	model "fps-app-ws-checklist-go/pkg/model"
	"log"
)

// ItemByChkList restrive one record by cod
func ItemByChkList(id int64) ([]model.Item,error) {
	items := []model.Item{}
	_, err := db.Dbmap.Select(&items, "Select * from item where cod_ch_list = ?", id)
	if err != nil {
		log.Println(err)
		return items, err
	}
	return items,nil
}

// ItemByChkList restrive one record by cod
func ItemByID(id int64) (model.Item,error) {
	item := model.Item{}
	err := db.Dbmap.SelectOne(&item, "Select * from item where cod_item = ?", id)
	if err != nil {
		log.Println(err)
		return item, err
	}
	return item,nil
}

// SaveItem grabar item
func SaveItem(item model.Item) (model.Item, error) {
	cant, err := db.Dbmap.SelectInt("select count(*) from item where cod_item = ?", item.CodItem)
	if err != nil {
		return item, err
	}
	if cant == 0 {
		return insertItem(item)
	} else {
		return updateItem(item)
	}
}

// insertItem insert into checklist
func insertItem(item model.Item) (model.Item, error) {
	item.Estado = 1 /* activo */
	queryInsert := `
	INSERT INTO item (cod_item, correosNotificacion, criticidad, enunciado, estado, longitudRespuesta, obligatorio, permiteComentarios, permiteDecimales, permiteImagen, 
		repeticionxTecnico, respuestaPositiva, tipoGrafico, tipoRespuesta, validoEnEstadisticas, cod_area, cod_cat, cod_ch_list, cod_sancion, cod_sub_cat) 
	VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`
	_, err := db.Dbmap.Exec(queryInsert,
		item.CodItem,
		item.CorreosNotificacion,
		item.Criticidad,
		item.Enunciado,
		item.Estado,
		item.LongitudRespuesta,
		item.Obligatorio,
		item.PermiteComentarios,
		item.PermiteDecimales,
		item.PermiteImagen,
		item.RepeticionxTecnico,
		item.RespuestaPositiva,
		item.TipoGrafico,
		item.TipoRespuesta,
		item.ValidoEnEstadisticas,
		item.CodArea,
		item.CodCat,
		item.CodChList,
		item.CodSancion,
		item.CodSubCat,
	)
	if err != nil {
		return item, err
	}
	if err != nil {
		return item, err
	}
	return item, nil
}

// updateItem actualizar Item
func updateItem(item model.Item) (model.Item, error) {
	queryUpdate := `
	UPDATE item 
		SET cod_ch_list = ?,
			correosNotificacion = ?,
			criticidad = ?,
			enunciado = ?,
			estado = ?,
			longitudRespuesta = ?,
			obligatorio = ?,
			permiteComentarios = ?,
			permiteDecimales = ?,
			permiteImagen = ?,
			repeticionxTecnico = ?,
			respuestaPositiva = ?,
			tipoGrafico = ?,
			tipoRespuesta = ?,
			validoEnEstadisticas = ?,
			cod_area = ?,
			cod_cat = ?,
			cod_sancion = ?,
			cod_sub_cat = ?
		where cod_item = ?`
	_, err := db.Dbmap.Exec(queryUpdate,
		item.CodChList,
		item.CorreosNotificacion,
		item.Criticidad,
		item.Enunciado,
		item.Estado,
		item.LongitudRespuesta,
		item.Obligatorio,
		item.PermiteComentarios,
		item.PermiteDecimales,
		item.PermiteImagen,
		item.RepeticionxTecnico,
		item.RespuestaPositiva,
		item.TipoGrafico,
		item.TipoRespuesta,
		item.ValidoEnEstadisticas,
		item.CodArea,
		item.CodCat,
		item.CodSancion,
		item.CodSubCat,
		item.CodItem,
	)
	if err != nil {
		return item, err
	}
	if err != nil {
		return item, err
	}
	return item, nil
}
