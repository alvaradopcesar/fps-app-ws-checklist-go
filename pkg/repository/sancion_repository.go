package repository

import (
	db "fps-app-ws-checklist-go/config"
	model "fps-app-ws-checklist-go/pkg/model"
)

// SancionFindAll a
func SancionFindAll() []model.Sancion {
	var sancion []model.Sancion
	db.Dbmap.Select(&sancion, "Select * from sancion ")
	return sancion
}
