package repository

import (
	db "fps-app-ws-checklist-go/config"
	model "fps-app-ws-checklist-go/pkg/model"
	"log"
)

// CategoriaFindAll a
func CategoriaFindAll() []model.Categoria {
	var categoria []model.Categoria
	_, err := db.Dbmap.Select(&categoria, "Select * from categoria where estado = 1")
	if err != nil {
		log.Println(err)
	}

	return categoria
}
