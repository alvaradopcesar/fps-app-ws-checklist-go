package repository

import (
	db "fps-app-ws-checklist-go/config"
	"fps-app-ws-checklist-go/pkg/model"
)

// UsuarioFindByCodUSer d
func UsuarioFindByCodUSer(codUsu string) (model.Usuario,error) {
	usuario := model.Usuario{}
	err := db.Dbmap.SelectOne(&usuario, `
		Select cod_usu, 
			login_usu,
			clave_usu,
			cod_perfil,
			cod_empresa,
			tipo
		from usuario 
		where login_usu = ?`,
	codUsu)
	if err != nil {
		return usuario,err
	}
	return usuario,nil
}

func UsuarioLocalList(nombreToFind string) ([]model.UsuarioLocalList, error ){
	var usuarioLocalList []model.UsuarioLocalList
	query :=
	`select usu.cod_empresa,
		cod_trab as codigo,
		tipo,
		CONCAT(trim(tra.nom_trab) , ' ', trim(tra.pat_trab) , ' ' , trim(tra.ape_mat_trab)) as nombre,
		car.desc_cargo
		from encuestas.usuario usu,
			ptoventa.ce_mae_trab tra,
			ptoventa.ce_cargo car
		where usu.cod_empresa = tra.cod_empresa
		and usu.cod_usu = tra.cod_trab
		and tra.cod_empresa = car.cod_empresa
		and tra.cod_cargo = car.cod_cargo
		and Upper(CONCAT(tra.nom_trab , ' ', tra.ape_pat_trab , ' ' , tra.ape_mat_trab)) like upper('%` + nombreToFind + `%') `
	_, err := db.Dbmap.Select(&usuarioLocalList , query )
	if err != nil {
		return usuarioLocalList, err
	}
	return usuarioLocalList,nil
}
