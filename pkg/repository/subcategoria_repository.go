package repository

import (
	db "fps-app-ws-checklist-go/config"
	model "fps-app-ws-checklist-go/pkg/model"
	"log"
)

// SubCategoriaFindAll d
func SubCategoriaFindAll() []model.SubCategoria {
	var subCategoria []model.SubCategoria
	_, err := db.Dbmap.Select(&subCategoria, "Select * from subcategoria where estado = 1")
	if err != nil {
		log.Println(err)
	}
	return subCategoria
}
