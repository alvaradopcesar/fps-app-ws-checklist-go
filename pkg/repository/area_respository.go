package repository

import (
	db "fps-app-ws-checklist-go/config"
	model "fps-app-ws-checklist-go/pkg/model"
	"log"
)

// AreaFindAll d
func AreaFindAll() []model.Area {
	var area []model.Area
	_, err := db.Dbmap.Select(&area, "Select * from area where estado = 1")
	if err != nil {
		log.Println(err)
	}
	return area
}
