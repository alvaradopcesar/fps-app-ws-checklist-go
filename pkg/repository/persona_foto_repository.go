package repository

import (
	db "fps-app-ws-checklist-go/config"
	"fps-app-ws-checklist-go/pkg/model"
)

// SavePersonaFoto grabar personaFoto
func SavePersonaFoto(personaFoto model.PersonaFoto) (model.PersonaFoto, error) {
	//cant, err := db.Dbmap.SelectInt("select count(*) from ptoventa.persona_foto where tip_doc_ident = ? and num_doc_iden = ? ", personaFoto.TipoDocIdent, personaFoto.NumDocIden)
	found, err := PersonaFotoExist(personaFoto)
	if err != nil {
		return personaFoto, err
	}
	if !found {
		return insertPersonaFoto(personaFoto)
	} else {
		return updatePersonaFoto(personaFoto)
	}
}


// insertPersonaFoto insert into personaFoto
func insertPersonaFoto(personaFoto model.PersonaFoto) (model.PersonaFoto, error) {
	queryInsert := `
	INSERT INTO ptoventa.persona_foto (tip_doc_ident, num_doc_iden, url_foto )
	VALUES (? ,?, ? )`
	_, err := db.Dbmap.Exec(queryInsert,
		personaFoto.TipoDocIdent,
		personaFoto.NumDocIden,
		personaFoto.UrlFoto)
	if err != nil {
		return personaFoto, err
	}
	if err != nil {
		return personaFoto, err
	}
	return personaFoto, nil
}

// UpdatePersonaFoto actualizar PersonaFoto
func updatePersonaFoto(personaFoto model.PersonaFoto) (model.PersonaFoto, error) {
	queryUpdate := `
	UPDATE ptoventa.persona_foto 
		set url_foto = ?,
            fec_mod = sysdate()
		where tip_doc_ident = ? 
          and num_doc_iden = ?`
	_, err := db.Dbmap.Exec(queryUpdate,
		personaFoto.UrlFoto,
		personaFoto.TipoDocIdent,
		personaFoto.NumDocIden)
	if err != nil {
		return personaFoto, err
	}
	return personaFoto, nil
}

func PersonaFotoExist(personaFoto model.PersonaFoto) (bool,error) {
	cant, err := db.Dbmap.SelectInt("select count(*) from ptoventa.persona_foto where tip_doc_ident = ? and num_doc_iden = ? ", personaFoto.TipoDocIdent, personaFoto.NumDocIden)
	if err != nil {
		return false, err
	}
	return cant>0,nil
}

func FingByCodigoPersonalFoto(personaFoto model.PersonaFoto) (model.PersonaFoto,error) {
	var personaFotoResponse model.PersonaFoto
	err := db.Dbmap.SelectOne(&personaFotoResponse,"select tip_doc_ident,num_doc_iden,url_foto from ptoventa.persona_foto where tip_doc_ident = ? and num_doc_iden = ? ", personaFoto.TipoDocIdent, personaFoto.NumDocIden)
	if err != nil {
		return personaFotoResponse,err
	}
	return personaFotoResponse,nil
}

