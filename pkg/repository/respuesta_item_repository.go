package repository

import (
	"fmt"
	"fps-app-ws-checklist-go/config"
	"fps-app-ws-checklist-go/pkg/model"
)

func RespouestaItemInsert( respuestaItem model.RespuestaItem ) (model.RespuestaItem,error) {
	respuestaItemResponse := model.RespuestaItem{}
	queryInsert := `
	INSERT INTO respuestaitem (comentarios, imagen, url, textoRespuesta, cod_item, opcionRespuesta_id, respuestachecklist_id)
	VALUES (?, ?, ?, ?, ?, ?, ?)`
	result, err := config.Dbmap.Exec(queryInsert,
		respuestaItem.Comentarios,
		respuestaItem.Imagen,
		respuestaItem.Url,
		respuestaItem.TextoRespuesta,
		respuestaItem.CodItem,
		respuestaItem.OpcionesRespuestaID,
		respuestaItem.RespuestaChecklistID,
	)
	if err != nil {
		return respuestaItemResponse, err
	}
	id , _ := result.LastInsertId()
	fmt.Println(id)
	respuestaItemResponse = respuestaItem
	respuestaItemResponse.ID = id
	return respuestaItemResponse, nil
}
