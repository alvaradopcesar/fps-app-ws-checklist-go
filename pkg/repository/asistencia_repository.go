package repository

import (
	db "fps-app-ws-checklist-go/config"
	"fps-app-ws-checklist-go/pkg/model"
	"log"
	"time"
)

// InsertAsistencia insert into asistencia
func AsistenciaInsert(asistencia model.Asistencia) (model.Asistencia, error) {
	queryInsert := `
	INSERT INTO asistencia (cod_empresa, cod_local, tip_doc_ident, num_doc_iden, tipo_operacion, fecha_operacion, latitud, longitud )
	VALUES (? ,?, ?, ?, ?, sysdate(), ?, ? )`
	res, err := db.Dbmap.Exec(queryInsert,
		asistencia.CodEmpresa,
		asistencia.CodLocal,
		asistencia.TipDocIdent,
		asistencia.NumDocIden,
		asistencia.TipoOperacion,
		asistencia.Latitud,
		asistencia.Longitud,
	)
	if err != nil {
		return asistencia, err
	}
	if err != nil {
		return asistencia, err
	}
	asistencia.ID,_ = res.LastInsertId()
	return asistencia, nil
}

// AsistenciaFindByDNI d
func AsistenciaFindByDNI(asistencia model.Asistencia) model.Asistencia {
	var asistenciaResult model.Asistencia
	err := db.Dbmap.SelectOne(&asistenciaResult, "Select * from asistencia where num_doc_iden = ? and tipo_operacion = ? and DATE(fecha_operacion) = DATE(sysdate())",
		asistencia.NumDocIden, asistencia.TipoOperacion)
	if err != nil {
		log.Println(err)
	}
	return asistenciaResult
}

func AsistenciaByFechas(fecIni,fecFin time.Time) ([]model.AsistenciaByFechasResponse,error) {
	asistenciaByFechasResponse := []model.AsistenciaByFechasResponse{}
	fecIniString := fecIni.Format("2006-01-02")
	fecFinString := fecFin.Format("2006-01-02")
	 sql := `
		select a.cod_empresa,
			a.cod_local,
			desc_local,
			a.tip_doc_ident,
			a.num_doc_iden,
			tr.cod_trab,
			CONCAT(trim(tr.nom_trab) , ' ', trim(tr.ape_pat_trab) , ' ' , trim(tr.ape_mat_trab)) as nom_trabajador,
			a.tipo_operacion,
			a.fecha_operacion
		from asistencia a
			left join ptoventa.pbl_local lo on lo.cod_empresa = a.cod_empresa and lo.cod_local = a.cod_local
        	left join ptoventa.ce_mae_trab tr on tr.cod_empresa = a.cod_empresa and tr.num_doc_iden = a.num_doc_iden
 		where a.fecha_operacion between STR_TO_DATE('`+ fecIniString + `', "%Y-%m-%D") and STR_TO_DATE('` + fecFinString +` 2359' , '%Y-%m-%d %H%i')
		`
	_, err := db.Dbmap.Select(&asistenciaByFechasResponse, sql)
	if err != nil {
		log.Println(err.Error())
		log.Println(sql)
		return asistenciaByFechasResponse, err
	}
	return asistenciaByFechasResponse, nil

}


