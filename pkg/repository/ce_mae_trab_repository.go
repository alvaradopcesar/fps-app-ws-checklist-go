package repository

import (
	db "fps-app-ws-checklist-go/config"
	"fps-app-ws-checklist-go/pkg/model"
)

// DniExist exite DNI
func DniExist(dni string) (bool,error) {
	cant, err := db.Dbmap.SelectInt("select count(*) from ptoventa.ce_mae_trab where num_doc_iden = ?", dni)
	if err != nil {
		return false,err
	}
	return cant>0,err
}

func TrabajdorByDNI(CodEmpresa,CodTrab string ) (model.TrabajadorLogin,error) {
	trabajador := model.TrabajadorLogin{}
	err := db.Dbmap.SelectOne(&trabajador,
			`select cod_empresa,
					cod_trab,
					concat(trim(nom_trab),' ',trim(ape_pat_trab),' ',trim(ape_mat_trab)) as nom_ape,
					num_doc_iden,
					tip_doc_ident
			from ptoventa.ce_mae_trab
			where cod_empresa = ?
		      and cod_trab = ? `,
		CodEmpresa,
		CodTrab)
	if err != nil {
		return trabajador,err
	}
	return trabajador,nil
}

