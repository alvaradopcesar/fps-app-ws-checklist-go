package repository

import (
	db "fps-app-ws-checklist-go/config"
	model "fps-app-ws-checklist-go/pkg/model"
	"log"
	"time"
)

// ChecklistFindAll a
func ChecklistFindAll() []model.Checklist {
	var checklist []model.Checklist
	db.Dbmap.Select(&checklist, "Select * from checklist order by fechacreacion desc")
	return checklist
}

// ChecklistFindByID restrive one record by cod
func ChecklistFindByID(id int64) model.Checklist {
	var checklist model.Checklist
	db.Dbmap.SelectOne(&checklist, "Select * from checklist where cod_ch_list = ?", id)
	return checklist
}

func ChecklistUserList(estado int32, CodUsu string ) ( []model.ChecklistUserList , error ){
	var checklistUserList []model.ChecklistUserList
	_, err := db.Dbmap.Select(&checklistUserList, `
		select a.cod_ch_list,a.titulo,a.estado,a.fechaCreacion, a.usuario_creacion,
				count(case when b.estado = 1 then 1 else null end) as respuestaParcial,
                count(case when b.estado = 2 then 1 else null end) as respuestaCompleta 
			  from checklist a 
					left outer join respuestachecklist b on a.cod_ch_list = b.cod_ch_list 
			  where a.estado = ? and a.cod_ch_list in (
				select cod_ch_list 
  					from checklist_usuario cu,
       					usuario u
  					where cu.cod_usu = u.cod_usu
    				and u.login_usu = ?
					)
		group by a.cod_ch_list,a.titulo,a.estado,a.fechaCreacion, a.usuario_creacion
	`, estado, CodUsu )
	if err != nil {
		return checklistUserList, err
	}
	return checklistUserList,nil
}

// SaveCheckList grabar checklist
func SaveCheckList(checklist model.Checklist) (model.Checklist, error) {
	cant, err := db.Dbmap.SelectInt("select count(*) from checklist where cod_ch_list = ?", checklist.CodChList)
	if err != nil {
		return checklist, err
	}
	if cant == 0 {
		return insertChecklist(checklist)
	} else {
		return updateChecklist(checklist)
	}
}

// insertChecklist insert into checklist
func insertChecklist(checklist model.Checklist) (model.Checklist, error) {
	queryInsert := `
	INSERT INTO checklist (correosNotificacion, guardaravance, msjbienvenida, msjfin, requiereaprobacionqf, requiereaprobaciont, requiereaprobacionu, 
		requierelocal, requiereqf, requieret, tiempovida, titulo, estado, fechacreacion ) 
	VALUES (? ,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`
	_, err := db.Dbmap.Exec(queryInsert,
		checklist.CorreosNotificacion,
		checklist.GuardarAvance,
		checklist.MsjBienvenida,
		checklist.MsjFin,
		checklist.RequiereAprobacionQF,
		checklist.RequiereAprobacionT,
		checklist.RequiereAprobacionU,
		checklist.RequiereLocal,
		checklist.RequiereQF,
		checklist.RequiereT,
		checklist.TiempoVida,
		checklist.Titulo,
		checklist.Estado,
		checklist.FechaCreacion,
	)
	if err != nil {
		return checklist, err
	}
	if err != nil {
		return checklist, err
	}
	return checklist, nil
}

// UpdateChecklist actualizar Checklist
func updateChecklist(checklist model.Checklist) (model.Checklist, error) {
	queryUpdate := `
	UPDATE checklist 
		set correosNotificacion = ?, 
			guardaravance = ?, 
			msjbienvenida = ?, 
			msjfin = ?, 
			requiereaprobacionqf = ?, 
			requiereaprobaciont = ?, 
			requiereaprobacionu = ?, 
			requierelocal = ?, 
			requiereqf = ?,
			requieret = ?, 
			tiempovida = ?, 
			titulo = ?,
			estado = ?, 
			fechacreacion = ?
		where cod_ch_list = ?`
	_, err := db.Dbmap.Exec(queryUpdate,
		checklist.CorreosNotificacion,
		checklist.GuardarAvance,
		checklist.MsjBienvenida,
		checklist.MsjFin,
		checklist.RequiereAprobacionQF,
		checklist.RequiereAprobacionT,
		checklist.RequiereAprobacionU,
		checklist.RequiereLocal,
		checklist.RequiereQF,
		checklist.RequiereT,
		checklist.TiempoVida,
		checklist.Titulo,
		checklist.Estado,
		checklist.FechaCreacion,
		checklist.CodChList,
	)
	if err != nil {
		return checklist, err
	}
	return checklist, nil
}

func ChecklistExportFormato01(fecIni,fecFin time.Time) ( []model.ChecklistExportFormato01, error ) {
	var checklistExportFormato01 []model.ChecklistExportFormato01
	fecIniString := fecIni.Format("2006-01-02")
	fecFinString := fecFin.Format("2006-01-02")
	sql := `
select cl.cod_ch_list,
	   cl.titulo,
	   it.cod_item,
       it.enunciado,
       IFNULL(it.tipoRespuesta, 0) as tipoRespuesta,
       CASE IFNULL(it.tipoRespuesta, 0)
           WHEN 0 THEN 'Opcion Simple'
		   WHEN 1 THEN 'Numerica'
           WHEN 2 THEN 'Abierta'
           WHEN 3 THEN 'Respuesta Multiple'
           WHEN 4 THEN 'Opcion Multiple'
       END as tipoRespuestaDesc,
       it.criticidad,
	   IFNULL(it.cod_area, 0) as cod_area,
	   IFNULL(a.nombre, "") as area,
	   IFNULL(it.cod_cat, 0) as cod_cat,
	   IFNULL(c.nombre, "") as categoria,
	   IFNULL(it.cod_sub_cat, 0) as cod_sub_cat,
	   IFNULL(sc.nombre, "") as subcategoria,
	   IFNULL(it.cod_sancion, 0) as cod_sancion,
	   IFNULL(s.descripcion, "") as sancion,
	   IFNULL(rcl.cod_usu_u, "") as cod_usu_u,
       concat(trim(IFNULL(tra.nom_trab,"")),' ',trim(IFNULL(tra.ape_pat_trab,"")), ' ' ,trim(IFNULL(tra.ape_mat_trab,""))) as nombre,
       rcl.fechaguardado,
	   IFNULL(rit.url, "") as url,
       IFNULL(rit.opcionRespuesta_id,0) as opcionRespuesta_id,
       ( select enunciado from opcionrespuesta op1 where op1.id = rit.opcionRespuesta_id ) as respuesta,
       IFNULL(riiopt.opcionRespuesta_id,0) as opcionRespuesta_id2,
       ( select enunciado from opcionrespuesta op1 where op1.id = riiopt.opcionRespuesta_id ) as respuesta2       
from encuestas.checklist cl
     join encuestas.item it on cl.cod_ch_list = it.cod_ch_list
     left join encuestas.area a on it.cod_area = a.cod_area
     left join encuestas.categoria c on it.cod_cat = c.cod_cat
     left join encuestas.subcategoria sc on it.cod_cat = c.cod_cat and it.cod_sub_cat = sc.cod_sub_cat
     left join encuestas.sancion s on it.cod_sancion = s.cod_sancion
     left join encuestas.respuestachecklist rcl on cl.cod_ch_list = rcl.cod_ch_list     
	 left join ptoventa.ce_mae_trab tra on tra.cod_trab = rcl.cod_usu_u
	 left join encuestas.respuestaitem rit on rit.cod_item = it.cod_item and rit.respuestachecklist_id = rcl.id 
     left join encuestas.respuestaitem_opcion riiopt on riiopt.Respuestaitem_id = rit.id
-- where not riiopt.opcionRespuesta_id is null
  --  where cl.cod_ch_list = 320331
  where rcl.fechaguardado between STR_TO_DATE('`+ fecIniString + `', "%Y-%m-%D") and STR_TO_DATE('` + fecFinString +` 2359' , '%Y-%m-%d %H%i')
order by cl.cod_ch_list,
	   it.cod_item,
       it.enunciado;
	`
	_, err := db.Dbmap.Select(&checklistExportFormato01, sql)
	if err != nil {
		log.Println(err.Error())
		log.Println(sql)
		return checklistExportFormato01, err
	}

	return checklistExportFormato01, nil
}

// ChecklistFindByCodUser restrive one record by cod_usu
func ChecklistFindByCodUser(cod_usu string) ([]model.Checklist,error) {
	checklist := []model.Checklist{}
	_,err := db.Dbmap.Select(&checklist, `
		select *
		from checklist cl
     inner join checklist_usuario u on u.cod_ch_list = cl.cod_ch_list and u.cod_usu = ?
	`,
	cod_usu)
	if err != nil {
		return checklist,err
	}
	return checklist,nil
}
