package repository

import (
	db "fps-app-ws-checklist-go/config"
	"fps-app-ws-checklist-go/pkg/model"
	"log"
)

//NivelUsuarioLocaByNivelID d
func NivelUsuarioLocaByNivelID(nivelID int64) []model.NivelUsuarioLocalLista {
	var nivelUsuarioLocalLista []model.NivelUsuarioLocalLista
	//Select * from nivel_usuario_local where estado = 1 and nivel_id =?
	sql := `
SELECT nul.id,
	   nul.nivel_id,
       nul.cod_empresa,
       nul.tipo,
       nul.codigo,
       CONCAT(tra.nom_trab , ' ', tra.ape_pat_trab , ' ' , tra.ape_mat_trab) as nombre,
       car.desc_cargo 
  FROM encuestas.nivel_usuario_local nul ,
	   ptoventa.ce_mae_trab tra,
       ptoventa.ce_cargo car
where nul.estado = 1
  and nul.nivel_id = ?
  and nul.tipo = 'USU'
  and nul.cod_empresa = tra.cod_empresa
  and nul.codigo = tra.cod_trab
  and tra.cod_empresa = car.cod_empresa
  and tra.cod_cargo = car.cod_cargo
union  
  SELECT nul.id,
	   nul.nivel_id,
       nul.cod_empresa,
       nul.tipo,
       nul.codigo,
       lca.desc_local as nombre,
       ' ' as desc_cargo
  FROM encuestas.nivel_usuario_local nul ,
	   ptoventa.pbl_local lca
where nul.estado = 1
  and nul.nivel_id = ?
  and nul.tipo = 'USU'
  and nul.codigo = lca.cod_local
	`
	_, err := db.Dbmap.Select(&nivelUsuarioLocalLista,sql ,nivelID,nivelID)
	if err != nil {
		log.Println(err)
	}
	return nivelUsuarioLocalLista
}


