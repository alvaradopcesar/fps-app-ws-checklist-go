package repository

import (
	"fmt"
	"fps-app-ws-checklist-go/config"
	"fps-app-ws-checklist-go/pkg/model"
	"log"
)

// ArbolFindAll d
func ArbolFindById(arbol model.Arbol) []model.Arbol {
	var arbolresponse []model.Arbol
	_, err := config.Dbmap.Select(&arbolresponse, "Select * from arbol where id = ?", arbol.ID)
	if err != nil {
		fmt.Println(err)
	}
	return arbolresponse
}

func ArbolFindByCodigo(arbol model.Arbol) ([]model.Arbol,error) {
	arbolResponse := []model.Arbol{}
	_, err := config.Dbmap.Select(&arbolResponse, "Select * from arbol where codigo = ?", arbol.Codigo)
	if err != nil {
		fmt.Println(err)
		return arbolResponse,err
	}
	return arbolResponse,nil
}

func ArbolFindByIdPadre(arbol model.Arbol) ([]model.Arbol,error) {
	arbolResponse := []model.Arbol{}
	_, err := config.Dbmap.Select(&arbolResponse, "Select * from arbol where id_padre = ?", arbol.IDPadre)
	if err != nil {
		fmt.Println(err)
		return arbolResponse,err
	}
	return arbolResponse,nil
}

func ArboGeolFindByIdPadre(arbol model.Arbol) ([]model.ArbolGeo,error) {
	arbolGeoresponse := []model.ArbolGeo{}
	_, err := config.Dbmap.Select(&arbolGeoresponse,
		`select id,id_padre,a.cod_empresa,a.tipo_usu_local,a.codigo,trim(l.desc_local) as desc_local,l.longitud,l.latitud
  			from arbol a
	   				inner join ptoventa.pbl_local l on l.cod_local = a.codigo and l.cod_empresa = a.cod_empresa and estado = 1 and not l.longitud is null
 			where id_padre = ?`,
		arbol.IDPadre)
	if err != nil {
		log.Println(err.Error())
		return arbolGeoresponse,err
	}
	return arbolGeoresponse,nil
}

func ArboGeolFindByIdPadreLista(idLista string) ([]model.ArbolGeo,error) {
	arbolGeoresponse := []model.ArbolGeo{}
	if idLista == "" {
		return arbolGeoresponse,nil
	}
	_, err := config.Dbmap.Select(&arbolGeoresponse,
		`select id,id_padre,a.cod_empresa,a.tipo_usu_local,a.codigo,trim(l.desc_local) as desc_local,l.longitud,l.latitud
  			from arbol a
	   				inner join ptoventa.pbl_local l on l.cod_local = a.codigo and l.cod_empresa = a.cod_empresa and estado = 1 and not l.longitud is null
 			where id_padre in (` + idLista + `) and tipo_usu_local = 'TDA'` )
	if err != nil {
		log.Println(err.Error())
		log.Println((idLista))
		return arbolGeoresponse,err
	}
	return arbolGeoresponse,nil
}


func ArbolFindByIdCodigo(arbol model.Arbol) ([]model.Arbol,error) {
	arbolresponse := []model.Arbol{}
	_, err := config.Dbmap.Select(&arbolresponse, "Select * from arbol where codigo = ?", arbol.Codigo)
	if err != nil {
		log.Println(err.Error())
		return arbolresponse,err
	}
	return arbolresponse,nil
}

func ArbolListOnlyPerson() ([]model.Arbol,error) {
	arbolresponse := []model.Arbol{}
	_, err := config.Dbmap.Select(&arbolresponse, "Select * from arbol where tipo_usu_local = 'PER'")
	if err != nil {
		log.Println(err.Error())
		return arbolresponse,err
	}
	return arbolresponse,nil
}

func ArbolListOnlyPersonWithNombre() ([]model.Arbol,error) {
	arbolresponse := []model.Arbol{}
	_, err := config.Dbmap.Select(&arbolresponse,`
		Select id,id_padre,a.cod_empresa,codigo,descripcion01, 
				CONCAT(trim(t.nom_trab) , ' ', trim(t.ape_pat_trab) , ' ' , trim(t.ape_mat_trab)) as descripcion02
  		from arbol a
       		left join ptoventa.ce_mae_trab t on a.cod_empresa = t.cod_empresa and a.codigo = t.num_doc_iden
 		where a.tipo_usu_local = 'PER'
   		and a.estado = 1`)
	if err != nil {
		log.Println(err.Error())
		return arbolresponse,err
	}
	return arbolresponse,nil
}