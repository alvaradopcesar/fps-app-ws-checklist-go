package model

// PBLLocal df
type PBLLocal struct {
	CodLocal     string `db:"cod_local" json:"cod_local"`
	CodCia       string `db:"cod_cia" json:"cod_cia"`
	DescLocal    string `db:"desc_local" json:"desc_local"`
	DirecLocal   string `db:"direc_local" json:"direc_local"`
	InnLocalProv string `db:"ind_local_prov" json:"ind_local_prov"`
	UbDep        string `db:"ubdep" json:"ubdep"`
	UbPrv        string `db:"ubprv" json:"ubprv"`
	UbDis        string `db:"ubdis" json:"ubdis"`
}
