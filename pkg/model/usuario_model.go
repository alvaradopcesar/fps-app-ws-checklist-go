package model

// Usuario tabla usuarios
type Usuario struct {
	CodUsu string `db:"cod_usu" json:"cod_usu"`
	LoginUsu string `db:"login_usu" json:"login_usu"`
	ClaveUsu string `db:"clave_usu" json:"clave_usu"`
	CodPerfil string `db:"cod_perfil" json:"cod_perfil"`
	CodEmpresa string `db:"cod_empresa" json:"cod_empresa"`
	Tipo string `db:"tipo" json:"tipo"`
}

type UsuarioLocalList struct {
	CodEmpresa string `db:"cod_empresa" json:"codEmpresa"`
	Codigo 	   string `db:"codigo" json:"codigo"`
	Tipo 	   string `db:"tipo" json:"tipo"`
	Nombre 	   string `db:"nombre" json:"nombre"`
	DescCargo  string `db:"desc_cargo" json:"descCargo"`
}

//select usu.cod_empresa,
//cod_trab as codigo,
//CONCAT(tra.nom_trab , ' ', tra.ape_pat_trab , ' ' , tra.ape_mat_trab) as nombre,
//car.desc_cargo
//from encuestas.usuario usu,
//ptoventa.ce_mae_trab tra,
//ptoventa.ce_cargo car
//where usu.cod_empresa = tra.cod_empresa
//and usu.cod_usu = tra.cod_trab
//and tra.cod_empresa = car.cod_empresa
//and tra.cod_cargo = car.cod_cargo;