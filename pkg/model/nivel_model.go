package model

import "time"

// Nivel sf
type Nivel struct {
	ID              int64      `db:"id" json:"id"`
	IDPadre  		*int64 	   `db:"id_padre" json:"idPadre"`
	Codigo          string     `db:"codigo" json:"codigo"`
	Descripcion01   string     `db:"descripcion01" json:"descripcion01"`
	Descripcion02   *string    `db:"descripcion02" json:"descripcion02"`
	Estado          *bool      `db:"estado" json:"estado"`
	FechaCreacion   *time.Time `db:"fechacreacion" json:"fechacreacion"`
	UsuarioCreacion *string    `db:"usuariocreacion" json:"usuariocreacion"`
}

//    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
//    `codigo` varchar(10) not null,
//    `codigo_padre` varchar(10) not null,
//    `descripcion01` varchar(100) not null,
//    `descripcion02` varchar(100) not null,
//    `estado` BOOLEAN,
//    `fechacreacion` DATETIME DEFAULT CURRENT_TIMESTAMP,
//    `usuariocreacion` VARCHAR(255),
//    PRIMARY KEY (`id`)
