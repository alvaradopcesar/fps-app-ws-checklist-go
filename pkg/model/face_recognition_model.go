package model

import "mime/multipart"

// UploadFileRequest df
type UploadFileRequest struct {
	CodEmpresa	string
	CodLocal	string
	Status      int
	UserCode 	string
	DNI			string
	FilePhoto   *multipart.FileHeader
}

type UploadFileResponse struct {
	Status  bool 	`json:"status"`
	ErrorCode int   `json:"errorCode"`
	Message string  `json:"message"`
}

type SubirImagenResponse struct {
	Success      bool   `json:"success"`
	Message      string `json:"message"`
	NombreImagen string `json:"nombreImagen"`
	URL          string `json:"url"`
}

// Structures for DETECT endpoint
type FaceRectangle struct {
	Top int `json:"top"`
	Left int `json:"left"`
	Width int `json:"width"`
	weight int `json:"height"`
}

type DetectResponseBody struct {
	FaceId string `json:"faceId"`
	FaceRectangle FaceRectangle `json:"faceRectangle"`
}


// Structures for VERIFY endpoint
type VerifyRequestBody struct {
	FaceId1      string   `json:"faceId1"`
	FaceId2      string   `json:"faceId2"`
}

type VerifyResponseBody struct {
	IsIdentical  bool `json:"isIdentical"`
	Confidence   int `json:"confidence"`
}