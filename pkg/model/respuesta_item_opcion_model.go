package model


type RespuestaItemOpcion struct {
	RespuestaItemId 	int64   `db:"RespuestaItem_id" json:"RespuestaItem_id"`
	OpcionrespuestaId 	int64   `db:"opcionrespuesta_id" json:"opcionrespuesta_id"`
}

//`RespuestaItem_id` bigint(20) NOT NULL,
//`opcionrespuesta_id` bigint(20) NOT NULL,
