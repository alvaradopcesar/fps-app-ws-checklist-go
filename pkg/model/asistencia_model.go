package model

import "time"

type Asistencia struct {
	ID             int64     `db:"id" json:"id"`
	CodEmpresa     string    `db:"cod_empresa" json:"cod_empresa"`
	CodLocal       string    `db:"cod_local" json:"cod_local"`
	TipDocIdent    string    `db:"tip_doc_ident" json:"tip_doc_ident"`
	NumDocIden     string    `db:"num_doc_iden" json:"num_doc_iden"`
	TipoOperacion  int       `db:"tipo_operacion" json:"tipo_operacion"`
	FechaOperacion time.Time `db:"fecha_operacion" json:"fecha_operacion"`
	Latitud        float64   `db:"latitud" json:"latitud"`
	Longitud       float64   `db:"longitud" json:"longitud"`
}

type AsistenciaByFechasResponse struct {
	//ID 				int64  `db:"id" json:"id"`
	CodEmpresa     string    `db:"cod_empresa" json:"cod_empresa"`
	CodLocal       string    `db:"cod_local" json:"cod_local"`
	DescLocal      string    `db:"desc_local" json:"desc_local"`
	TipDocIdent    string    `db:"tip_doc_ident" json:"tip_doc_ident"`
	NumDocIden     string    `db:"num_doc_iden" json:"num_doc_iden"`
	CodTrabajador  string    `db:"cod_trab" json:"cod_trab"`
	NomTrabajador  string    `db:"nom_trabajador" json:"nom_trabajador"`
	TipoOperacion  int       `db:"tipo_operacion" json:"tipo_operacion"`
	FechaOperacion time.Time `db:"fecha_operacion" json:"fecha_operacion"`
}
