package model

import "time"

// NivelUsuarioLocal sf
type NivelUsuarioLocal struct {
	ID              int64      `db:"id" json:"id"`
	NivelId  		*int64 	   `db:"nivel_id" json:"nivelId"`
	CodEmpresa      *string    `db:"cod_empresa" json:"codEmpresa"`
	Tipo   			string     `db:"tipo" json:"tipo"`
	Codigo   		string     `db:"codigo" json:"codigo"`
	Estado          *bool      `db:"estado" json:"estado"`
	FechaCreacion   *time.Time `db:"fechacreacion" json:"fechacreacion"`
}

type NivelUsuarioLocalLista struct {
	ID              int64      `db:"id" json:"id"`
	NivelId  		*int64 	   `db:"nivel_id" json:"nivelId"`
	CodEmpresa      *string    `db:"cod_empresa" json:"codEmpresa"`
	Tipo   			string     `db:"tipo" json:"tipo"`
	Codigo   		string     `db:"codigo" json:"codigo"`
	Nombre   		string     `db:"nombre" json:"nombre"`
	DescCargo   	*string     `db:"desc_cargo" json:"descCargo"`
}

//SELECT nul.id,
//nul.nivel_id,
//nul.cod_empresa,
//nul.tipo,
//nul.codigo,
//CONCAT(tra.nom_trab , ' ', tra.ape_pat_trab , ' ' , tra.ape_mat_trab) as nombres,
//car.desc_cargo

//`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
//`nivel_id` BIGINT(20) null,
//`cod_empresa` varchar(10) DEFAULT NULL,
//`tipo` varchar(3) null,
//`codigo` varchar(15) NOT NULL,
//`estado` BOOLEAN,
//`fechacreacion` DATETIME DEFAULT CURRENT_TIMESTAMP,