package model

type PersonaFoto struct {
	TipoDocIdent string `db:"tip_doc_ident" json:"tip_doc_ident"`
	NumDocIden 	 string `db:"num_doc_iden" json:"num_doc_iden"`
	UrlFoto      string `db:"url_foto" json:"url_foto"`
}

//CREATE TABLE `persona_foto` (
//`tip_doc_ident` varchar(2) NOT NULL,
//`num_doc_iden` varchar(30) NOT NULL,
//`url_foto` varchar(120) NOT NULL,
//`fec_crea` datetime DEFAULT CURRENT_TIMESTAMP,
//`fec_mod` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
//PRIMARY KEY (`num_doc_iden`,`tip_doc_ident`)
//) ENGINE=InnoDB DEFAULT CHARSET=utf8;
