package model

// Item sf
type OpcionRespuesta struct {
	ID 			int64   `db:"id" json:"id"`
	Connotacion int64   `db:"connotacion" json:"connotacion"`
	Enunciado   *string `db:"enunciado" json:"enunciado"`
	CodItem     int64   `db:"cod_item" json:"cod_item"`
	Selected    bool    `db:"selected" json:"selected"`
}

//`id` bigint(20) NOT NULL AUTO_INCREMENT,
//`connotacion` int(11) DEFAULT NULL,
//`enunciado` varchar(255) DEFAULT NULL,
//`cod_item` bigint(20) DEFAULT NULL,

type OpcionRespuestaListarReponse struct {
	ID 			int64   `json:"id"`
	Enunciado   string  `json:"enunciado"`
	Connotacion int64   `json:"connotacion"`
}

