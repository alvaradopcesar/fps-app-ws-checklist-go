package model

import (
	"database/sql"
	"time"
)

// Checklist df
type Checklist struct {
	CodChList            int64     `db:"cod_ch_list" json:"codChList"`
	Titulo               string     `db:"titulo" json:"titulo"`
	GuardarAvance        *bool      `db:"guardaravance" json:"guardaravance"`
	MsjBienvenida        *string    `db:"msjbienvenida" json:"msjbienvenida"`
	MsjFin               *string    `db:"msjfin" json:"msjfin"`
	UsuarioCreacion      *string    `db:"usuario_creacion" json:"usuarioCreacion"`
	CorreosNotificacion  *string    `db:"correosNotificacion" json:"correosNotificacion"`
	RequiereAprobacionQF bool      `db:"requiereaprobacionqf" json:"requiereaprobacionqf"`
	RequiereAprobacionT  bool      `db:"requiereaprobaciont" json:"requiereaprobaciont"`
	RequiereAprobacionU  bool      `db:"requiereaprobacionu" json:"requiereaprobacionu"`
	RequiereLocal        bool       `db:"requierelocal" json:"requierelocal"`
	RequiereQF           bool       `db:"requiereqf" json:"requiereqf"`
	RequiereT            bool      `db:"requieret" json:"requieret"`
	TiempoVida           int32      `db:"tiempovida" json:"tiempovida"`
	Estado               int32      `db:"estado" json:"estado"`
	FechaCreacion        *time.Time `db:"fechacreacion" json:"fechacreacion"`
	Items                []Item     `json:"items"`
	Users                []ChecklistUsuario `json:"usuarios"`
}

// CodGrupoCiaCreacion  string `db:"cod_grupo_cia_creacion" json:"cod_grupo_cia_creacion"`
// `fechacreacion` datetime DEFAULT CURRENT_TIMESTAMP,
// `cod_grupo_cia_creacion` varchar(255) DEFAULT NULL,
// `sec_usu_local_creacion` varchar(255) DEFAULT NULL,

type ChecklistUserList struct {
	CodChList            int64     `db:"cod_ch_list" json:"codChList"`
	Titulo               string     `db:"titulo" json:"titulo"`
	Estado               int32      `db:"estado" json:"estado"`
	FechaCreacion        *time.Time `db:"fechacreacion" json:"fechacreacion"`
	UsuarioCreacion      *string    `db:"usuario_creacion" json:"usuarioCreacion"`
	RespuestaParcial	 int32 		`db:"respuestaParcial" json:"respuestaParcial"`
	RespuestaCompleta	 int32 		`db:"respuestaCompleta" json:"respuestaCompleta"`

}

//select a.cod_ch_list,a.titulo,a.estado,a.fechaCreacion, a.usuario_creacion,
//count(case when b.estado = 1 then 1 else null end),
//count(case when b.estado = 2 then 1 else null end)

type ChecklistExportFormato01 struct {
	CodChList            string     `db:"cod_ch_list" json:"codChList"`
	Titulo               string     `db:"titulo" json:"titulo"`
	CodItem              int64   `db:"cod_item" json:"cod_item"`
	Enunciado            string  `db:"enunciado" json:"enunciado"`
	TipoRespuesta        int32  `db:"tipoRespuesta" json:"tipoRespuesta"`
	TipoRespuestaDesc    string  `db:"tipoRespuestaDesc" json:"tipoRespuestaDesc"`
	Criticidad           int32   `db:"criticidad" json:"criticidad"`
	CodArea              int64  `db:"cod_area" json:"cod_area"`
	Area    	         string  `db:"area" json:"area"`
	CodCat               int64  `db:"cod_cat" json:"cod_cat"`
	Categoria            string  `db:"categoria" json:"categoria"`
	CodSubCat            int64  `db:"cod_sub_cat" json:"cod_sub_cat"`
	SubCategoria         string  `db:"subcategoria" json:"subcategoria"`
	CodSancion           int64  `db:"cod_sancion" json:"cod_sancion"`
	Sancion           	 string  `db:"sancion" json:"sancion"`
	CodUsu           	 string  `db:"cod_usu_u" json:"codusu"`
	Nombre           	 string  `db:"nombre" json:"nombre"`
	FechaGuardado        sql.NullTime `db:"fechaguardado" json:"fechaguardado"`
	Url           	 	 string  `db:"url" json:"url"`
	OpcionRespuestaId    int64  `db:"opcionRespuesta_id" json:"opcionRespuesta_id"`
	Respuesta            sql.NullString  `db:"respuesta" json:"respuesta"`
	OpcionRespuestaId2   int64  `db:"opcionRespuesta_id2" json:"opcionRespuesta_id2"`
	Respuesta2           sql.NullString  `db:"respuesta2" json:"respuesta2"`
}

//select cl.cod_ch_list,
//cl.titulo,
//it.cod_item,
//it.enunciado,
//it.tipoRespuesta,
//CASE it.tipoRespuesta
//WHEN 0 THEN 'Opcion Simple'
//		   WHEN 1 THEN 'Numerica'
//           WHEN 2 THEN 'Abierta'
//           WHEN 3 THEN 'Respuesta Multiple'
//           WHEN 4 THEN 'Opcion Multiple'
//       END as tipoRespuestaDesc,
//it.criticidad,
//it.cod_area,
//a.nombre as area,
//it.cod_cat,
//c.nombre as categoria,
//it.cod_sub_cat,
//sc.nombre as subcategoria,
//it.cod_sancion,
//s.descripcion as sancion,
//rcl.cod_usu_u,
//concat(trim(tra.nom_trab),' ',trim(tra.ape_pat_trab), ' ' ,trim(tra.ape_mat_trab)) as nombre,
//rcl.fechaguardado,
//rit.url,
//rit.opcionRespuesta_id,
//( select enunciado from opcionrespuesta op1 where op1.id = rit.opcionRespuesta_id ) as respuesta,
//riiopt.opcionRespuesta_id,
//( select enunciado from opcionrespuesta op1 where op1.id = riiopt.opcionRespuesta_id ) as respuesta2

type ChecklistListarResponse struct {
	Id 						int64 	`json:"id"`
	TiempoVida  			int32   `json:"tiempoVida"`
	TiempoVidaIni  			int32   `json:"tiempoVidaIni"`
	TiempoVidaFin  			int32   `json:"tiempoVidaFin"`
	RequiereLocal  			bool    `json:"requiereLocal"`
	RequiereQf  			bool    `json:"requiereQF"`
	RquiereAprobacionQF  	bool    `json:"requiereAprobacionQF"`
	CorreosNotificacion  	string  `json:"correosNotificacion"`
	FechaCreacion        	int64 	`json:"fechaCreacion"`
	UsuarioCreacion      	string  `json:"usuarioCreacion"`
	Estado               	int32   `json:"estado"`
	Titulo               	string  `json:"nombre"`
	MsjBienvenida        	string  `json:"bienvenida"`
	MsjFin               	string  `json:"finalizacion"`
	RequiereAvance          bool    `json:"requiereAvance"`
	RequiereT          		bool    `json:"requiereTecnico"`
	RequiereAprobacionT  	bool    `json:"requiereAprobacionTecnico"`
	RequiereAprobacionU  	bool    `json:"requiereAprobacionUsuario"`
	Items					[]ItemListarReponse `json:"items"`
}

