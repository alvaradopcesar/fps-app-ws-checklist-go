package model

// Item sf
type Item struct {
	CodChList            int64   `db:"cod_ch_list" json:"codChList"`
	CodItem              int64   `db:"cod_item" json:"cod_item"`
	CorreosNotificacion  *string `db:"correosNotificacion" json:"correosNotificacion"`
	Criticidad           int32   `db:"criticidad" json:"criticidad"`
	Enunciado            string  `db:"enunciado" json:"enunciado"`
	CodItemSec         	 *int64  `db:"cod_item_sec" json:"codItemSec"`
	Orden         	     *int32  `db:"orden" json:"orden"`
	Estado               int32   `db:"estado" json:"estado"`
	LongitudRespuesta    *int32  `db:"longitudRespuesta" json:"longitudRespuesta"`
	Obligatorio          *bool   `db:"obligatorio" json:"obligatorio"`
	PermiteComentarios   *bool   `db:"permiteComentarios" json:"permiteComentarios"`
	PermiteDecimales     *bool   `db:"permiteDecimales" json:"permiteDecimales"`
	PermiteImagen        *bool   `db:"permiteImagen" json:"permiteImagen"`
	RepeticionxTecnico   *bool   `db:"repeticionxTecnico" json:"repeticionxTecnico"`
	RespuestaPositiva    *int32  `db:"respuestaPositiva" json:"respuestaPositiva"`
	TipoGrafico          *int32  `db:"tipoGrafico" json:"tipoGrafico"`
	TipoRespuesta        *int32  `db:"tipoRespuesta" json:"tipoRespuesta"`
	ValidoEnEstadisticas *bool   `db:"validoEnEstadisticas" json:"validoEnEstadisticas"`
	CodArea              *int64  `db:"cod_area" json:"cod_area"`
	CodCat               *int64  `db:"cod_cat" json:"cod_cat"`
	CodSancion           *int64  `db:"cod_sancion" json:"cod_sancion"`
	CodSubCat            *int64  `db:"cod_sub_cat" json:"cod_sub_cat"`
	OpcionesRespuesta	 []OpcionRespuesta `json:"opcionesRespuesta"`
	RespuestaItem		 RespuestaItem `json:"respuestaItem"`
}

type ItemListarReponse struct {
	CodItem              int64  `json:"id"`
	Enunciado            string `json:"enunciado"`
	Obligatorio          bool   `json:"obligatorio"`
	Criticidad           int32  `json:"criticidad"`
	RespuestaPositiva    int32  `json:"respuestaPositiva"`
	PermiteComentarios   bool   `json:"permiteComentarios"`
	Estado               int32  `json:"estado"`
	PermiteDecimales     bool   `json:"permiteDecimales"`
	ValidoEnEstadisticas bool   `json:"validoEnEstadisticas"`
	TipoRespuesta        int32  `json:"tipo"`
	PermiteImagen        bool   `json:"requiereImagen"`
	Repeticion  		 bool   `json:"repeticion"`
	OpcionRespuesta 	[]OpcionRespuestaListarReponse `json:"opciones"`
}

