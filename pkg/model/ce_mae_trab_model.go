package model

type TrabajadorLogin struct {
	CodEmpresa string `db:"cod_empresa"`
	NomApe     string `db:"nom_ape"`
	CodTrab    string `db:"cod_trab"`
	NumDocIden string `db:"num_doc_iden"`
	TipDocIdent string `db:"tip_doc_ident"`
}

