package model

// PBLUsuarioLocal s
type PBLUsuarioLocal struct {
	// `clave_usu` string `db:"clave_usu" json:"clave_usu"`
	EstUsu   string `db:"est_usu" json:"est_usu"`
	LoginUsu string `db:"login_usu" json:"login_usu"`
	Rodlocal string `db:"cod_local" json:"cod_local"`
	CodUsu   string `db:"cod_usu" json:"cod_usu"`
}
