package model

import "time"

// Checklist df
type ChecklistUsuario struct {
	CodChList          string `db:"cod_ch_list" json:"codChList"`
	CodUsu             string `db:"cod_usu" json:"codUsu"`
	FecCreacion        *time.Time  `db:"fec_crea" json:"fecCreacion"`
	FecModificacion    *time.Time  `db:"fec_mod" json:"fecModificacion"`
}

//`cod_ch_list` bigint(20) NOT NULL,
//`cod_usu` varchar(30) NOT NULL,
//`fec_crea` datetime DEFAULT CURRENT_TIMESTAMP,
//`fec_mod` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
