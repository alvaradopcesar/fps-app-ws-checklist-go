package model

import "time"

// Arbol df
type Arbol struct {
	ID              int64     `db:"id" json:"id"`
	IDPadre         int64     `db:"id_padre" json:"id_padre"`
	CodEmpresa      string    `db:"cod_empresa" json:"cod_empresa"`
	TipoUsuLocal    string    `db:"tipo_usu_local" json:"tipo_usu_local"`
	Codigo          string    `db:"codigo" json:"codigo"`
	Descripcion01   *string   `db:"descripcion01" json:"descripcion01"`
	Descripcion02   *string   `db:"descripcion02" json:"descripcion02"`
	Estado          int32     `db:"estado" json:"estado"`
	FechaCreacion   time.Time `db:"fechacreacion" json:"fechacreacion"`
	UsuarioCreacion *string   `db:"usuariocreacion" json:"usuariocreacion"`
}

//`id` bigint(20) NOT NULL AUTO_INCREMENT,
//`id_padre` bigint(20) NOT NULL,
//`cod_empresa` varchar(5) NOT NULL,
//`tipo_usu_local` varchar(3) NOT NULL,
//`codigo` varchar(10) NOT NULL,
//`descripcion01` varchar(100) DEFAULT NULL,
//`descripcion02` varchar(100) DEFAULT NULL,
//`estado` tinyint(1) DEFAULT NULL,
//`fechacreacion` datetime DEFAULT CURRENT_TIMESTAMP,
//`usuariocreacion` varchar(100) DEFAULT NULL,

type ArbolGeo struct {
	ID           int64   `db:"id" json:"id"`
	IDPadre      int64   `db:"id_padre" json:"id_padre"`
	CodEmpresa   string  `db:"cod_empresa" json:"cod_empresa"`
	TipoUsuLocal string  `db:"tipo_usu_local" json:"tipo_usu_local"`
	Codigo       string  `db:"codigo" json:"codigo"`
	Descripcion  string  `db:"desc_local" json:"desc_local"`
	Latitud      float64 `json:"latitud"`
	Longitud     float64 `json:longitud"`
}

type ArbolGeoReturn struct {
	ID          int64  `db:"id" json:"id"`
	CodEmpresa  string `db:"cod_empresa" json:"cod_empresa"`
	Codigo      string `db:"codigo" json:"codigo"`
	Descripcion string `db:"desc_local" json:"desc_local"`
}
