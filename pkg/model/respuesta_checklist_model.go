package model

import (
	"database/sql"
)

type RespuestaChecklist struct {
	ID                int64          `db:"id" json:"id"`
	AprobacionQF      bool           `db:"aprobacionqf" json:"aprobacionqf"`
	AprobacionT       bool           `db:"aprobacionT" json:"aprobacionT"`
	AprobacionU       bool           `db:"aprobacionU" json:"aprobacionU"`
	Estado            int32          `db:"estado" json:"estado"`
	fechaCreacion     sql.NullTime   `db:"fechaCreacion" json:"fechaCreacion"`
	FechaFinalizacion sql.NullTime   `db:"fechaFinalizacion" json:"fechaFinalizacion"`
	CodChList         int64          `db:"cod_ch_list" json:"cod_ch_list"`
	CodUsu            string         `db:"cod_usu_u" json:"cod_usu_u"`
	FechaGuardado     sql.NullTime   `db:"fechaguardado" json:"fechaguardado"`
	VersionApp        sql.NullString `db:"versionApp" json:"versionApp"`
}
//CREATE TABLE `respuestachecklist` (
//`id` bigint(20) NOT NULL AUTO_INCREMENT,
//`aprobacionqf` tinyint(1) DEFAULT NULL,
//`aprobacionT` tinyint(1) DEFAULT NULL,
//`aprobacionU` tinyint(1) DEFAULT NULL,
//`creadoDesde` int(11) DEFAULT NULL,
//`estado` int(11) DEFAULT NULL,
//`fechaCreacion` datetime DEFAULT NULL,
//`fechaFinalizacion` datetime DEFAULT NULL,
//`finalizadoDesde` int(11) DEFAULT NULL,
//`cod_ch_list` bigint(20) DEFAULT NULL,
//`cod_local` char(3) DEFAULT NULL,
//`cod_local_QF` char(3) DEFAULT NULL,
//`cod_usu_QF` varchar(30) DEFAULT NULL,
//`cod_local_T` char(3) DEFAULT NULL,
//`cod_usu_T` varchar(30) DEFAULT NULL,
//`cod_local_u` char(3) DEFAULT NULL,
//`cod_usu_u` varchar(30) DEFAULT NULL,
//`fechaguardado` datetime DEFAULT NULL,
//`versionApp` varchar(255) DEFAULT NULL,
//PRIMARY KEY (
