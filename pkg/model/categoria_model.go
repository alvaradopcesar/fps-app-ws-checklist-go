package model

// Categoria df
type Categoria struct {
	CodCat      int64  `db:"cod_cat" json:"codCat"`
	Nombre      string `db:"nombre" json:"nombre"`
	AreaCodArea int64  `db:"area_cod_area" json:"areaCodArea"`
	Estado      int32  `db:"estado" json:"estado"`
}

// `cod_cat` bigint(20) NOT NULL AUTO_INCREMENT,
// `estado` int(11) DEFAULT NULL,
// `nombre` varchar(255) DEFAULT NULL,
// `area_cod_area` bigint(20) DEFAULT NULL,
