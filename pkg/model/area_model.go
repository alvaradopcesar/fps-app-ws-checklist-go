package model

// Area df
type Area struct {
	CodArea int64  `db:"cod_area" json:"codArea"`
	Nombre  string `db:"nombre" json:"nombre"`
	Estado  int32  `db:"estado" json:"estado"`
}

// `cod_area` bigint(20) NOT NULL AUTO_INCREMENT,
// `estado` int(11) DEFAULT NULL,
// `nombre` varchar(255) DEFAULT NULL,
