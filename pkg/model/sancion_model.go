package model

// Sancion sf
type Sancion struct {
	CodSancion  int64  `db:"cod_sancion" json:"cod_sancion"`
	Descripcion string `db:"descripcion" json:"descripcion"`
}

// 	`cod_sancion` bigint(20) NOT NULL AUTO_INCREMENT,
//   `descripcion` varchar(255) DEFAULT NULL,
//   `estado` int(11) DEFAULT NULL,
//   `nombre` varchar(255) DEFAULT NULL,
