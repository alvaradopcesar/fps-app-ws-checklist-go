package model

import "database/sql"

// RespuestaItem df
type RespuestaItem struct {
	ID 					 int64   `db:"id" json:"id"`
	Comentarios     	 string  `db:"comentarios" json:"comentarios"`
	Imagen          	 string  `db:"imagen" json:"imagen"`
	Url             	 string  `db:"url" json:"url"`
	TextoRespuesta       string  `db:"textoRespuesta" json:"textoRespuesta"`
	CodItem              int64   `db:"cod_item" json:"codItem"`
	OpcionesRespuestaID  sql.NullInt64   `db:"opcionRespuesta_id" json:"opcionRespuestaId"`
	RespuestaChecklistID int64   `db:"respuestachecklist_id" json:"respuestaChecklistId"`
}

//`id` bigint(20) NOT NULL AUTO_INCREMENT,
//`comentarios` varchar(255) DEFAULT NULL,
//`imagen` varchar(255) DEFAULT NULL,
//`url` varchar(5000) DEFAULT NULL,
//`textoRespuesta` varchar(255) DEFAULT NULL,
//`cod_item` bigint(20) DEFAULT NULL,
//`opcionRespuesta_id` bigint(20) DEFAULT NULL,
//`respuestachecklist_id` bigint(20) DEFAULT NULL,
