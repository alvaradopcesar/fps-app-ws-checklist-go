package model

// SubCategoria df
type SubCategoria struct {
	CodSubCat int64  `db:"cod_sub_cat" json:"codSubCat"`
	Nombre    string `db:"nombre" json:"nombre"`
	CodCat    int64  `db:"cod_cat" json:"codCat"`
	Estado    int32  `db:"estado" json:"estado"`
}

// `cod_sub_cat` bigint(20) NOT NULL AUTO_INCREMENT,
// `estado` int(11) DEFAULT NULL,
// `nombre` varchar(255) DEFAULT NULL,
// `cod_cat` bigint(20) DEFAULT NULL,
