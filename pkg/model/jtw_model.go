package model

// JWTUser sabla usuaruios
type JWTUser struct {
	UserName string `db:"username" json:"username"`
	Password string `db:"password" json:"password"`
}

// JWTUserMobil sabla usuaruios
type JWTUserMobil struct {
	UserName string `db:"login" json:"login"`
	Password string `db:"clave" json:"clave"`
}
