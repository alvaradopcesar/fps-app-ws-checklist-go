package service

import (
	"database/sql"
	"fmt"
	"fps-app-ws-checklist-go/pkg/model"
	"fps-app-ws-checklist-go/pkg/repository"
	"log"
)

type ChecklistResponse struct {
	Status  bool 	`json:"status"`
	Message string  `json:"message"`
}

func RespuestaChecklistSave(codUsu,versionApp string,checklist model.Checklist) ChecklistResponse {
	respuestaChecklistResponse, err := repository.RespouestaChecklistInsert(model.RespuestaChecklist{
		ID:                0,
		AprobacionQF:      false,
		AprobacionT:       false,
		AprobacionU:       false,
		Estado:            1,
		CodChList:         checklist.CodChList,
		CodUsu:            codUsu,
		VersionApp:        sql.NullString{String:versionApp,Valid:true},
	})
	if err != nil {
		log.Println(err.Error())
		return ChecklistResponse{false,err.Error()}
	}
	for _ , item := range checklist.Items {
		fmt.Println(item)
		opcionesRespuestaID :=  sql.NullInt64{}
		if *item.TipoRespuesta == 0 || *item.TipoRespuesta == 4 {
			for _, opcionRespuesta := range item.OpcionesRespuesta {
				if opcionRespuesta.Selected {
					opcionesRespuestaID = sql.NullInt64{opcionRespuesta.ID,true}
				}
			}
		}
		respuestaItemResponse, err := repository.RespouestaItemInsert(model.RespuestaItem{
			Comentarios:          item.RespuestaItem.Comentarios,
			Imagen:               item.RespuestaItem.Imagen,
			Url:                  item.RespuestaItem.Url,
			TextoRespuesta:       item.RespuestaItem.TextoRespuesta,
			CodItem:              item.CodItem,
			OpcionesRespuestaID:  opcionesRespuestaID,
			RespuestaChecklistID: respuestaChecklistResponse.ID,
		})
		if err != nil {
			log.Println(err.Error())
			return ChecklistResponse{false,err.Error()}
		}
		if *item.TipoRespuesta == 3 {
			for _, opcionRespuesta := range item.OpcionesRespuesta {
				if opcionRespuesta.Selected {
					_, err := repository.RespouestaItemOpcionInsert(model.RespuestaItemOpcion{
						RespuestaItemId:   respuestaItemResponse.ID,
						OpcionrespuestaId: opcionRespuesta.ID,
					})
					if err != nil {
						log.Println(err.Error())
						return ChecklistResponse{false,err.Error()}
					}
				}
			}
		}
	}

	return ChecklistResponse{true,"Ok"}
}

type ChecklistMobilRequest struct {
	CodChList            int64      `db:"cod_ch_list" json:"checklist"`
	Estado               int32      `db:"estado" json:"estado"`
	//FechaFinalizacion    *time.Time `db:"Fechacreacion" json:"fechaFinalizacion"`
	Respuestas  []checklistMobilItemRequest  `json:"respuestas"`
}

type checklistMobilItemRequest struct {
	CodItem              		int64   `json:"item"`
	ImagenEnviada        		bool   `json:"imagenEnviada"`
	OpcionRespuesta      		opcionRespuestaRequest `json:"opcionRespuesta"`
	OpcionesRespuestaMultiple 	[]opcionRespuestaMultipleRequest `json:""opcionesRespuestaMultiple""`
}

type opcionRespuestaRequest struct {
	ID			 int64    	`json:"id"`
	Connotacion  int64   	`json:"connotacion"`
	Enunciado	 string 	`json:"enunciado"`
}

type opcionRespuestaMultipleRequest struct {
	ID			 int64    	`json:"id"`
}


type ChecklistMobileResponse struct {
	Status  bool 	`json:"status"`
	Message string  `json:"message"`
}

func RespuestaChecklistSaveMobil(codUsu,versionApp string,checklistMobilRequest ChecklistMobilRequest) ChecklistMobileResponse {
	respuestaChecklistResponse, err := repository.RespouestaChecklistInsert(model.RespuestaChecklist{
		ID:                0,
		AprobacionQF:      false,
		AprobacionT:       false,
		AprobacionU:       false,
		Estado:            1,
		CodChList:         checklistMobilRequest.CodChList,
		CodUsu:            codUsu,
		VersionApp:        sql.NullString{String:versionApp,Valid:true},
	})
	if err != nil {
		log.Println(err.Error())
		return ChecklistMobileResponse{false,err.Error()}
	}
	for _ , item := range checklistMobilRequest.Respuestas {
		fmt.Println(item)
		opcionesRespuestaID :=  sql.NullInt64{}

		// Obtener el tipo de respuesta
		itemT ,_ := repository.ItemByID(item.CodItem)
		if *itemT.TipoRespuesta == 3 {
			for _, opcionRespuesta := range item.OpcionesRespuestaMultiple {
				opcionesRespuestaID = sql.NullInt64{opcionRespuesta.ID, true}
			}
		}else{
			opcionesRespuestaID = sql.NullInt64{item.OpcionRespuesta.ID, true}
		}
		respuestaItemResponse, err := repository.RespouestaItemInsert(model.RespuestaItem{
			Comentarios:          "",
			Imagen:               "",
			Url:                  "",
			TextoRespuesta:       "",
			CodItem:              item.CodItem,
			OpcionesRespuestaID:  opcionesRespuestaID,
			RespuestaChecklistID: respuestaChecklistResponse.ID,
		})
		if err != nil {
			log.Println(err.Error())
			return ChecklistMobileResponse{false,err.Error()}
		}
		if *itemT.TipoRespuesta == 3 {
			for _, opcionRespuesta := range item.OpcionesRespuestaMultiple {
					_, err := repository.RespouestaItemOpcionInsert(model.RespuestaItemOpcion{
						RespuestaItemId:   respuestaItemResponse.ID,
						OpcionrespuestaId: opcionRespuesta.ID,
					})
					if err != nil {
						log.Println(err.Error())
						return ChecklistMobileResponse{false,err.Error()}
					}
			}
		}
	}

	return ChecklistMobileResponse{true, "OK" }
}