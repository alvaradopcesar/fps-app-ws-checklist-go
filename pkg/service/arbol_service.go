package service

import (
	"fps-app-ws-checklist-go/pkg/model"
	"fps-app-ws-checklist-go/pkg/repository"
	"strconv"
)

type ArbolTreeNode struct {
	Data ArbolData `json:"data"`
	Children []ArbolTreeNode `json:"children"`
	Leaf bool `json:"feaf"`
	Expanded bool `json:"expamded"`
}

type ArbolData struct {
	ID int64  `db:"id" json:"id"`
	IDPadre int64  `db:"id_padre" json:"id_padre"`
	CodEmpresa string  `db:"cod_empresa" json:"cod_empresa"`
	TipoUsuLocal string  `db:"tipo_usu_local" json:"tipo_usu_local"`
	Codigo string  `db:"codigo" json:"codigo"`
	Descripcion01 string  `db:"descripcion01" json:"descripcion01"`
	Descripcion02 *string  `db:"descripcion02" json:"descripcion02"`
}

func GetArbolTree(id_padre int64) []ArbolTreeNode {
	arbolFull, err :=repository.ArbolListOnlyPersonWithNombre()
	if err != nil {
		return []ArbolTreeNode{}
	}
	return ArmaArbol(id_padre ,arbolFull )
}

func ArmaArbol(id int64, arbolFull []model.Arbol ) []ArbolTreeNode {
	arbolTreeNodes := []ArbolTreeNode{}
	arbolList := seekChild(id,arbolFull)
	for _, data := range arbolList {
		var arbolTreeNode ArbolTreeNode
		arbolTreeNode.Data.ID = data.ID
		arbolTreeNode.Data.IDPadre = data.IDPadre
		arbolTreeNode.Data.CodEmpresa = data.CodEmpresa
		arbolTreeNode.Data.Codigo = data.Codigo
		arbolTreeNode.Children = ArmaArbol(data.ID,arbolFull)
		arbolTreeNode.Data.Descripcion01 = *data.Descripcion01
		arbolTreeNode.Data.Descripcion02 = data.Descripcion02
		arbolTreeNodes = append(arbolTreeNodes, arbolTreeNode)
	}
	return arbolTreeNodes
}

func GetLocalesByDNI(dni string)  ([]model.ArbolGeo,error) {
	arbolFull, err :=repository.ArbolListOnlyPerson()
	if err != nil {
		return []model.ArbolGeo{},err
	}
	arboln1,err := repository.ArbolFindByCodigo(model.Arbol{Codigo:dni})
	if err != nil {
		return []model.ArbolGeo{},err
	}
	arbolResponse := []model.Arbol{}
	arbolResponse = arboln1;
	for _,n1 := range arboln1 {
		arbolResponse = append( arbolResponse ,  getChilds(n1.ID, arbolFull)... )
	}
	filtro := ""
	first := true
	for _, data := range arbolResponse {
		if first {
			filtro = filtro + strconv.Itoa(int(data.ID))
		}else {
			filtro = filtro + "," + strconv.Itoa(int(data.ID))
		}
		first = false
	}
	return repository.ArboGeolFindByIdPadreLista(filtro)
}

func getChilds( id int64, arbolFull []model.Arbol) []model.Arbol {
	arbolResponse := []model.Arbol{}
	arbolList := seekChild(id,arbolFull)
	for _, data := range arbolList {
		if data.TipoUsuLocal == "PER" {
			arbolResponse = append( arbolResponse , data)
			arbolResponse = append( arbolResponse ,  getChilds(data.ID, arbolFull)... )
		}
	}
	return arbolResponse
}

func seekChild(id int64,arbolFull []model.Arbol) []model.Arbol {
	arbolResponse := []model.Arbol{}
	for _, data := range arbolFull {
		if data.IDPadre == id {
			arbolResponse = append(arbolResponse, data)
		}
	}
	return arbolResponse
}
