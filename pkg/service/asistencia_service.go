package service

import (
	"fps-app-ws-checklist-go/pkg/model"
	"log"

	//"fps-app-ws-checklist-go/pkg/model"
	//"fps-app-ws-checklist-go/pkg/repository"
	"fps-app-ws-checklist-go/pkg/repository"
	"time"
)

type AsistenciaByType struct {
	Marcado bool `db:"marcado" json:"marcado"`
	TipoOperacion 	int  `db:"tipo_operacion" json:"tipo_operacion"`
	FechaOperacion 	time.Time  `db:"fecha_operacion" json:"fecha_operacion"`
}

// AsistenciaList lista all
func AsistenciaList(dni string) []AsistenciaByType {
	asistenciaByTypeList := []AsistenciaByType{}
	asistencia1 := repository.AsistenciaFindByDNI(model.Asistencia{NumDocIden:dni,TipoOperacion:1})
	if (model.Asistencia{}) == asistencia1  {
		asistenciaByTypeList = append(asistenciaByTypeList, AsistenciaByType{Marcado:false, TipoOperacion: 1, FechaOperacion: asistencia1.FechaOperacion})
	}else {
		asistenciaByTypeList = append(asistenciaByTypeList, AsistenciaByType{Marcado:true, TipoOperacion: asistencia1.TipoOperacion, FechaOperacion: asistencia1.FechaOperacion})
	}

	asistencia2 := repository.AsistenciaFindByDNI(model.Asistencia{NumDocIden:dni,TipoOperacion:2})
	if (model.Asistencia{}) == asistencia2  {
		asistenciaByTypeList = append(asistenciaByTypeList, AsistenciaByType{Marcado:false, TipoOperacion: 2, FechaOperacion: asistencia1.FechaOperacion})
	}else {
		asistenciaByTypeList = append(asistenciaByTypeList, AsistenciaByType{Marcado:true, TipoOperacion: asistencia2.TipoOperacion, FechaOperacion: asistencia2.FechaOperacion})
	}

	asistencia3 := repository.AsistenciaFindByDNI(model.Asistencia{NumDocIden:dni,TipoOperacion:3})
	if (model.Asistencia{}) == asistencia3  {
		asistenciaByTypeList = append(asistenciaByTypeList, AsistenciaByType{Marcado:false, TipoOperacion: 3, FechaOperacion: asistencia3.FechaOperacion})
	}else {
		asistenciaByTypeList = append(asistenciaByTypeList, AsistenciaByType{Marcado:true, TipoOperacion: asistencia3.TipoOperacion, FechaOperacion: asistencia3.FechaOperacion})
	}

	asistencia4 := repository.AsistenciaFindByDNI(model.Asistencia{NumDocIden:dni,TipoOperacion:4})
	if (model.Asistencia{}) == asistencia4  {
		asistenciaByTypeList = append(asistenciaByTypeList, AsistenciaByType{Marcado:false, TipoOperacion: 4, FechaOperacion: asistencia1.FechaOperacion})
	}else {
		asistenciaByTypeList = append(asistenciaByTypeList, AsistenciaByType{Marcado:true, TipoOperacion: asistencia4.TipoOperacion, FechaOperacion: asistencia4.FechaOperacion})
	}

	return asistenciaByTypeList
}

type AsistenciaMarcacionRequest struct {
	CodLocal 		string  `json:"cod_local"`
	TipoOperacion 	int  	`json:"tipo_operacion"`
	Latitud        float64  `json:"latitud"`
	Longitud       float64  `json:"longitud"`
}

type AsistenciaMarcacionResponse struct {
	Status  bool 	`json:"status"`
	ErrorCode int   `json:"errorCode"`
	Message string  `json:"message"`
}


func AsistenciaMarcacion( dni string , asistencia AsistenciaMarcacionRequest ) AsistenciaMarcacionResponse {
	_, err := repository.AsistenciaInsert(model.Asistencia{
		CodEmpresa:     "",
		CodLocal:       asistencia.CodLocal,
		TipDocIdent:    "01",
		NumDocIden:     dni,
		TipoOperacion:  asistencia.TipoOperacion,
		Latitud:		asistencia.Latitud,
		Longitud:		asistencia.Longitud,
	})
	if err != nil {
		log.Println("AsistenciaMarcacion :" ,err.Error())
		return AsistenciaMarcacionResponse{
			Status:    false,
			ErrorCode: 200,
			Message:   "No se registro la marcacion correctamente",
		}
	}

	respond := PostMarcacionRemote(MarcacionRemoteRequest{
		CodGrupoCia: "001",
		CodLocal:    asistencia.CodLocal,
		Dni:         dni,
	})
	if !respond.Status {
		log.Println(respond)
	}

	return AsistenciaMarcacionResponse{
		Status:    true,
		ErrorCode: 0,
		Message:   "Ok",
	}
}
