package service

import (
	"fmt"
	"fps-app-ws-checklist-go/pkg/repository"
	"fps-app-ws-checklist-go/util"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"

	"github.com/360EntSecGroup-Skylar/excelize"
	"log"
	"time"
)

func ExportFileTemplate01(fecIni,fecFin time.Time) (string,string, error) {
	createDirectoryAndDeleteoldFiles("tmp")
	t := time.Now()
	template := "./template_excel/template01.xlsx"
	finalFileName := "excel_template01_" + t.Format("2006-01-02_150405") + ".xlsx"
	finalFile := "./tmp/" + finalFileName
	err := util.Copy(template,finalFile,1000)
	if err != nil {
		log.Println(err.Error())
		return "","",err
	}
	err = cargarExcelFileTemplate01(fecIni,fecFin,finalFile)
	if err != nil {
		return "","",err
	}
	return finalFile,finalFileName,nil
}


func cargarExcelFileTemplate01(fecIni,fecFin time.Time,fileName string) error {

	xlFile, err := excelize.OpenFile(fileName)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	checklistExportFormat01, err := repository.ChecklistExportFormato01(fecIni,fecFin)
	if err != nil {
		return err
	}
	for x,data := range checklistExportFormat01 {
		xlFile.SetCellValue("Data", "A"+ strconv.Itoa(x+2), data.CodChList)
		xlFile.SetCellValue("Data", "B"+ strconv.Itoa(x+2), data.Titulo)
		xlFile.SetCellValue("Data", "C"+ strconv.Itoa(x+2), data.CodItem)
		xlFile.SetCellValue("Data", "D"+ strconv.Itoa(x+2), data.Enunciado)
		xlFile.SetCellValue("Data", "E"+ strconv.Itoa(x+2), data.TipoRespuesta)
		xlFile.SetCellValue("Data", "F"+ strconv.Itoa(x+2), data.TipoRespuestaDesc)
		xlFile.SetCellValue("Data", "G"+ strconv.Itoa(x+2), data.Criticidad)
		xlFile.SetCellValue("Data", "H"+ strconv.Itoa(x+2), data.CodArea)
		xlFile.SetCellValue("Data", "I"+ strconv.Itoa(x+2), data.Area)
		xlFile.SetCellValue("Data", "J"+ strconv.Itoa(x+2), data.CodCat)
		xlFile.SetCellValue("Data", "K"+ strconv.Itoa(x+2), data.Categoria)
		xlFile.SetCellValue("Data", "L"+ strconv.Itoa(x+2), data.CodSubCat)
		xlFile.SetCellValue("Data", "M"+ strconv.Itoa(x+2), data.SubCategoria)
		xlFile.SetCellValue("Data", "N"+ strconv.Itoa(x+2), data.CodSancion)
		xlFile.SetCellValue("Data", "O"+ strconv.Itoa(x+2), data.Sancion)
		xlFile.SetCellValue("Data", "P"+ strconv.Itoa(x+2), data.CodUsu)
		xlFile.SetCellValue("Data", "Q"+ strconv.Itoa(x+2), data.Nombre)
		if data.FechaGuardado.Valid {
			xlFile.SetCellValue("Data", "R"+strconv.Itoa(x+2), data.FechaGuardado.Time)
		}
		xlFile.SetCellValue("Data", "S"+ strconv.Itoa(x+2), data.Url)
		xlFile.SetCellValue("Data", "T"+ strconv.Itoa(x+2), data.OpcionRespuestaId)
		xlFile.SetCellValue("Data", "U"+ strconv.Itoa(x+2), data.Respuesta.String)
		xlFile.SetCellValue("Data", "V"+ strconv.Itoa(x+2), data.OpcionRespuestaId2)
		xlFile.SetCellValue("Data", "W"+ strconv.Itoa(x+2), data.Respuesta2.String)
	}
	err = xlFile.Save()
	if err != nil {
		fmt.Printf(err.Error())
		return err
	}
	return nil
}

func createDirectoryAndDeleteoldFiles(directory string) error {
	newpath := filepath.Join(".", directory)
	os.MkdirAll(newpath, os.ModePerm)
	files, err := ioutil.ReadDir("./"+directory)
	if err != nil {
		return err
	}
	for _, file := range files {
		_, _, d1 := file.ModTime().Date()
		_, _, d2 := time.Now().Date()
		if d1 - d2 != 0 {
			fileDelete := directory+"/"+file.Name()
			err := os.Remove(fileDelete)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func ExportFileTemplate02(fecIni,fecFin time.Time) (string,string, error) {
	createDirectoryAndDeleteoldFiles("tmp")
	t := time.Now()
	template := "./template_excel/template02.xlsx"
	finalFileName := "excel_template02_" + t.Format("2006-01-02_150405") + ".xlsx"
	finalFile := "./tmp/" + finalFileName
	err := util.Copy(template,finalFile,1000)
	if err != nil {
		log.Println(err.Error())
		return "","",err
	}
	err = cargarExcelFileTemplate02(fecIni,fecFin,finalFile)
	if err != nil {
		return "","",err
	}
	return finalFile,finalFileName,nil
}

func cargarExcelFileTemplate02(fecIni,fecFin time.Time,fileName string) error {
	xlFile, err := excelize.OpenFile(fileName)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	checklistExportFormat01, err := repository.AsistenciaByFechas(fecIni,fecFin)
	if err != nil {
		return err
	}
	for x,data := range checklistExportFormat01 {
		xlFile.SetCellValue("Data", "A"+ strconv.Itoa(x+2), data.CodEmpresa)
		xlFile.SetCellValue("Data", "B"+ strconv.Itoa(x+2), data.CodLocal)
		xlFile.SetCellValue("Data", "C"+ strconv.Itoa(x+2), data.DescLocal)
		xlFile.SetCellValue("Data", "D"+ strconv.Itoa(x+2), data.TipDocIdent)
		xlFile.SetCellValue("Data", "E"+ strconv.Itoa(x+2), data.NumDocIden)
		xlFile.SetCellValue("Data", "F"+ strconv.Itoa(x+2), data.CodTrabajador)
		xlFile.SetCellValue("Data", "G"+ strconv.Itoa(x+2), data.NomTrabajador)
		xlFile.SetCellValue("Data", "H"+ strconv.Itoa(x+2), data.TipoOperacion)
		xlFile.SetCellValue("Data", "I"+ strconv.Itoa(x+2), data.FechaOperacion)
	}
	err = xlFile.Save()
	if err != nil {
		fmt.Printf(err.Error())
		return err
	}
	return nil
}
