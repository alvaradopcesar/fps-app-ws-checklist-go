package service

import (
	"errors"
	"fps-app-ws-checklist-go/pkg/model"
	"fps-app-ws-checklist-go/pkg/repository"
	"math"
)

type LocalByPersonRequest struct {
	DNI string `json:"dni"`
	Latitud float64 `json:"latitud"`
	Longitud float64 `json:"longitud"`
}

func GetLocalByPerson(localByPersonRequest LocalByPersonRequest) ([]model.ArbolGeoReturn,error) {
	arbolList,_ := repository.ArbolFindByIdCodigo(model.Arbol{Codigo:localByPersonRequest.DNI})
	arbolReturn := []model.ArbolGeo{}
	arbolGeoReturn := []model.ArbolGeoReturn{}
	if len(arbolList) == 0 {
		return arbolGeoReturn,errors.New("Persona no tiene reistrada gerarquia !!")
	}
	for _, arbol := range arbolList {
		arbolReturn = findChildArbol(arbol.ID)
	}

	arbolGeoReturn = CalculaDistanceLocal(localByPersonRequest,arbolReturn)

	return arbolGeoReturn,nil
}

func findChildArbol(IDPadre int64) []model.ArbolGeo {
	arbolGeoReturn := []model.ArbolGeo{}
	arbolGeoList,_ := repository.ArboGeolFindByIdPadre(model.Arbol{IDPadre:IDPadre})
	for _, arbolGeo := range arbolGeoList {
		if arbolGeo.TipoUsuLocal == "TDA" {
			arbolGeoReturn = append(arbolGeoReturn, arbolGeo)
		}else {
			arbolGeoReturn = findChildArbol(arbolGeo.ID)
		}

	}
	return arbolGeoReturn
}

func CalculaDistanceLocal(localByPersonRequest LocalByPersonRequest,arbolGeo []model.ArbolGeo,) []model.ArbolGeoReturn {
	arbolGeoReturn := []model.ArbolGeoReturn{}
	for _,data := range arbolGeo {
		metros := Distance(data.Latitud, data.Longitud, localByPersonRequest.Latitud, localByPersonRequest.Longitud, "m")
		if metros <= 500 {
		arbolGeoReturn = append(arbolGeoReturn, model.ArbolGeoReturn{
			ID:          data.ID,
			CodEmpresa:  data.CodEmpresa,
			Codigo:      data.Codigo,
			Descripcion: data.Descripcion,
			})
		}
	}
	return arbolGeoReturn
}

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:::                                                                         :::
//:::  This routine calculates the distance between two points (given the     :::
//:::  latitude/longitude of those points). It is being used to calculate     :::
//:::  the distance between two locations using GeoDataSource (TM) prodducts  :::
//:::                                                                         :::
//:::  Definitions:                                                           :::
//:::    South latitudes are negative, east longitudes are positive           :::
//:::                                                                         :::
//:::  Passed to function:                                                    :::
//:::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
//:::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
//:::    unit = the unit you desire for results                               :::
//:::           where: 'M' is statute miles (default)                         :::
//:::                  'K' is kilometers                                      :::
//:::                  'N' is nautical miles                                  :::
//:::                                                                         :::
//:::  Worldwide cities and other features databases with latitude longitude  :::
//:::  are available at https://www.geodatasource.com                         :::
//:::                                                                         :::
//:::  For enquiries, please contact sales@geodatasource.com                  :::
//:::                                                                         :::
//:::  Official Web site: https://www.geodatasource.com                       :::
//:::                                                                         :::
//:::               GeoDataSource.com (C) All Rights Reserved 2018            :::
//:::                                                                         :::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

func Distance(lat1 float64, lng1 float64, lat2 float64, lng2 float64, unit ...string) float64 {
	const PI float64 = 3.141592653589793

	radlat1 := float64(PI * lat1 / 180)
	radlat2 := float64(PI * lat2 / 180)

	theta := float64(lng1 - lng2)
	radtheta := float64(PI * theta / 180)

	dist := math.Sin(radlat1) * math.Sin(radlat2) + math.Cos(radlat1) * math.Cos(radlat2) * math.Cos(radtheta)

	if dist > 1 {
		dist = 1
	}

	dist = math.Acos(dist)
	dist = dist * 180 / PI
	dist = dist * 60 * 1.1515

	if len(unit) > 0 {
		if unit[0] == "K" {
			dist = dist * 1.609344
		} else if unit[0] == "N" {
			dist = dist * 0.8684
		} else if unit[0] == "m" {
			dist = ( dist * 1.609344 ) * 1000
		}
	}

	return dist
}
