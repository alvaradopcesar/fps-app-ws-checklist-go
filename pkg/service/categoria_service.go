package service

import (
	"fps-app-ws-checklist-go/pkg/model"
	"fps-app-ws-checklist-go/pkg/repository"
)

// CategoriaFindAll lista all
func CategoriaFindAll() []model.Categoria {
	return repository.CategoriaFindAll()
}
