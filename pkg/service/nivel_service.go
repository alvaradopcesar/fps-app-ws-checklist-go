package service

import (
	"fps-app-ws-checklist-go/pkg/model"
	"fps-app-ws-checklist-go/pkg/repository"
	"log"
)

// TodoItemNode a
type TodoItemNode struct {
	ID       int64          `json:"id"`
	Code     string         `json:"code"`
	Item     string         `json:"item"`
	Children []TodoItemNode `json:"children"`
}

// NivelFindAll nivel all
func NivelFindAll() []TodoItemNode {
	var todoItemNodeList []TodoItemNode
	nivel := repository.NivelAllRoot()
	var todoItemNodeRootList []TodoItemNode
	var todoItemNodeRoot TodoItemNode
	todoItemNodeRoot.ID = 0
	todoItemNodeRoot.Code = "0"
	todoItemNodeRoot.Item = "Raiz"

	for _, y := range nivel {
		var todoItemNode TodoItemNode
		todoItemNode.ID = y.ID
		todoItemNode.Code = y.Codigo
		todoItemNode.Item = y.Descripcion01
		todoItemNode.Children = NivelFindByPadre(y.ID)
		todoItemNodeList = append(todoItemNodeList, todoItemNode)
	}
	todoItemNodeRoot.Children = todoItemNodeList
	todoItemNodeRootList = append(todoItemNodeRootList, todoItemNodeRoot)
	return todoItemNodeRootList
}

// NivelFindByPadre d
func NivelFindByPadre(IDPadre int64) []TodoItemNode {
	var todoItemNodeList []TodoItemNode
	nivel := repository.NivelAllByPadre(IDPadre)
	for _, y := range nivel {
		var todoItemNode TodoItemNode
		todoItemNode.ID = y.ID
		todoItemNode.Code = y.Codigo
		todoItemNode.Item = y.Descripcion01
		todoItemNode.Children = NivelFindByPadre(y.ID)
		todoItemNodeList = append(todoItemNodeList, todoItemNode)
	}
	return todoItemNodeList
}

// NivelSave save checklist
func NivelSave(nivel model.Nivel) model.Nivel {
	nivel, err := repository.NivelSave(nivel)
	if err != nil {
		log.Println(err)
	}
	return nivel
}

func NivelUsuarioLocaByNivelID(nivelID int64) []model.NivelUsuarioLocalLista {
	return repository.NivelUsuarioLocaByNivelID(nivelID)
}

