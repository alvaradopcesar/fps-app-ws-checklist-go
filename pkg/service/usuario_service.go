package service

import (
	"fps-app-ws-checklist-go/pkg/model"
	"fps-app-ws-checklist-go/pkg/repository"
)

// SancionFindAll sancion all
func UsuarioLocalList(nombreToFind string) ([]model.UsuarioLocalList,error) {
	return repository.UsuarioLocalList(nombreToFind)
}
