package service

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"fps-app-ws-checklist-go/pkg/model"
	"fps-app-ws-checklist-go/pkg/repository"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/pkg/errors"
	"log"
	"net/http"
	"strings"
	"time"
)

func Login(c echo.Context,passEncryp bool) error {

	jwtUser := new(model.JWTUserMobil)

	if err := c.Bind(jwtUser); err != nil {
		return err
	}
	user, err := repository.UsuarioFindByCodUSer(jwtUser.UserName)
	if (model.Usuario{}) == user {
		fmt.Println("usuario no Existe !! " + jwtUser.UserName)
		return echo.ErrUnauthorized
	}

	if passEncryp {
		if getStringEncript(user.ClaveUsu) != jwtUser.Password {
			fmt.Println("password errado !! ")
			return echo.ErrUnauthorized
		}
	} else {
		if user.ClaveUsu != jwtUser.Password {
			fmt.Println("password errado !! ")
			return echo.ErrUnauthorized
		}
	}

	trabajador := model.TrabajadorLogin{}
	tienda := model.PBLLocal{}
	if user.Tipo == "TDA" {
		tienda.CodCia = user.CodEmpresa
		tienda.CodLocal = "001"
		tienda.DescLocal = "Tienda 001"
		tienda.DirecLocal = " Direccion 001"
	} else {
		trabajador, err = repository.TrabajdorByDNI(user.CodEmpresa, user.CodUsu)
		if err != nil {
			fmt.Println(err.Error())
		}
	}


	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["cod_user"] = user.CodUsu
	claims["name"] = trabajador.NomApe
	claims["perfil"] = user.CodPerfil
	claims["dni"] = trabajador.NumDocIden
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte("secret"))
	if err != nil {
		return err
	}

	sessionKey := ""
	AndroidVersion := c.Request().Header.Get("X-Version-App")
	if AndroidVersion != "" {
		sessionKey = getStringEncript(user.LoginUsu + time.Now().String())[0:32]
	}

	response := LoginResponse{}
	if user.Tipo == "TDA" {
		response = LoginResponse{
			Success:    true,
			SessionKey: sessionKey,
			Valid:      true,
			Token:      t,
			Usuario: UsuarioLoginResponse{
				Login:      user.LoginUsu,
				Perfil:     user.CodPerfil,
				CodUsu:     user.CodUsu,
				CodEmpresa: user.CodEmpresa,
				Trabajador: TrabajadorLoginReponse{
					NomApe:                   tienda.DescLocal,
					TipoDocumentoIdentidad:   "",
					NumeroDocumentoIdentidad: "",
				},
			},
		}
	}else{
		response = LoginResponse{
			Success:    true,
			SessionKey: sessionKey,
			Valid:      true,
			Token:      t,
			Usuario: UsuarioLoginResponse{
				Login:      user.LoginUsu,
				Perfil:     user.CodPerfil,
				CodUsu:     user.CodUsu,
				CodEmpresa: trabajador.CodEmpresa,
				Trabajador: TrabajadorLoginReponse{
					NomApe:                   trabajador.NomApe,
					TipoDocumentoIdentidad:   trabajador.TipDocIdent,
					NumeroDocumentoIdentidad: trabajador.NumDocIden,
				},
			},
		}
	}
	return c.JSON(http.StatusOK, response)

}

func getStringEncript(cadena string) string {
	h := sha256.New()
	h.Write([]byte(cadena))
	return hex.EncodeToString(h.Sum(nil))
}

func ExtractClaims(tokenStr string) (jwt.MapClaims, bool) {
	hmacSecretString := "secret"

	splitToken := strings.Split(tokenStr, "Bearer ")
	tokenStr = splitToken[1]

	hmacSecret := []byte(hmacSecretString)
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		// check token signing method etc
		return hmacSecret, nil
	})

	if err != nil {
		return nil, false
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, true
	} else {
		log.Printf("Invalid JWT Token")
		return nil, false
	}
}

func GetDniFromToken(tokenStr string) (string,error) {
	claims, Ok := ExtractClaims(tokenStr)
	if !Ok {
		return "", errors.New("Error obteniendo Codigo usuario")
	}
	return claims["dni"].(string), nil
}

func GetCodUSerFromToken(tokenStr string) (string,error) {
	claims, Ok := ExtractClaims(tokenStr)
	if !Ok {
		return "", errors.New("Error obteniendo Codigo usuario")
	}
	return claims["cod_user"].(string), nil
}



type LoginResponse struct {
	Success bool `json:"success"`
	Usuario UsuarioLoginResponse `json:"usuario"`
	SessionKey string `json:"sessionKey"`
	Valid      bool   `json:"valid"`
	Token      string `json:"token"`
}

type UsuarioLoginResponse struct {
	Trabajador TrabajadorLoginReponse `json:"trabajador"`
	Login      string `json:"login"`
	Perfil     string `json:"perfil"`
	CodEmpresa string `json:"codEmpresa"`
	CodUsu     string `json:"cod_usu"`
}

type TrabajadorLoginReponse struct {
	NomApe		 				string `json:"nomApe"`
	TipoDocumentoIdentidad  	string `json:"tipoDocumentoIdentidad"`
	NumeroDocumentoIdentidad    string `json:"numeroDocumentoIdentidad"`
}
