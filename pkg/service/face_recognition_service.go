package service

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"fps-app-ws-checklist-go/config"
	"fps-app-ws-checklist-go/pkg/model"
	"fps-app-ws-checklist-go/pkg/repository"
	"github.com/labstack/gommon/log"
	"io/ioutil"
	"mime/multipart"
	"net/http"
)

func ExisteDNI(uploadFile model.UploadFileRequest) model.UploadFileResponse {
	exist, err := repository.DniExist(uploadFile.DNI)
	if err != nil {
		return model.UploadFileResponse{Status:false,ErrorCode:100,Message:err.Error()}
	}
	if !exist {
		return model.UploadFileResponse{Status:false,ErrorCode:200,Message:"DNI No existe"}
	}
	return model.UploadFileResponse{Status:true,ErrorCode:0,Message:"Si existe"}
}

func ExistePhoto(uploadFile model.UploadFileRequest) model.UploadFileResponse {
	uploadFileResponse := ExisteDNI(uploadFile)
	if !uploadFileResponse.Status {
		return uploadFileResponse
	}
	exist, err := repository.PersonaFotoExist(model.PersonaFoto{ TipoDocIdent :"01", NumDocIden : uploadFile.DNI})
	if err != nil {
		return model.UploadFileResponse{Status:false,ErrorCode:400,Message:err.Error()}
	}
	if !exist {
		return model.UploadFileResponse{Status:false,ErrorCode:500,Message:"Foto No existe !!"}
	}
	return model.UploadFileResponse{Status:true,ErrorCode:600,Message:"Foto si existe"}
}

func UploadFile(uploadFile model.UploadFileRequest) model.UploadFileResponse {
	exist, err := repository.DniExist(uploadFile.DNI)
	if err != nil {
		return model.UploadFileResponse{Status:false,ErrorCode:700,Message:err.Error()}
	}
	if !exist {
		return model.UploadFileResponse{Status:false,ErrorCode:800,Message:"DNI No existe"}
	}

	switch uploadFile.Status {

	case 0:
		subirImagenResponse, err := loadAzureStorage(config.CONFIG.AzureConfig.DataStorage.Container.ContainerChecklist,uploadFile.FilePhoto)
		if  err != nil {
			return model.UploadFileResponse{Status:false,ErrorCode:900,Message:err.Error()}
		}
		if subirImagenResponse.Success {
			_, err := repository.SavePersonaFoto(model.PersonaFoto{TipoDocIdent:"01",NumDocIden: uploadFile.DNI, UrlFoto:subirImagenResponse.URL})
			if err != nil {
				return model.UploadFileResponse{Status:false,ErrorCode:1000,Message:err.Error()}
			}
		}
		return model.UploadFileResponse{Status:subirImagenResponse.Success,ErrorCode:0,Message:""}

	case 1,2,3,4:
		found,err := repository.PersonaFotoExist(model.PersonaFoto{TipoDocIdent:"01",NumDocIden:uploadFile.DNI})
		if err != nil {
			return model.UploadFileResponse{Status:false,ErrorCode:1100,Message:err.Error()}
		}
		if !found {
			return model.UploadFileResponse{Status:false,ErrorCode:1200,Message: "Primero debera registrar su foto"}
		}
		personalFotoOriginal, err := repository.FingByCodigoPersonalFoto(model.PersonaFoto{TipoDocIdent:"01",NumDocIden:uploadFile.DNI})
		if err != nil {
			return model.UploadFileResponse{Status:false,ErrorCode:1300,Message:err.Error()}
		}

		subirImagenResponse , err := loadAzureStorage(config.CONFIG.AzureConfig.DataStorage.Container.ContainerTmp,uploadFile.FilePhoto)
		if err != nil {
			return model.UploadFileResponse{Status:false,ErrorCode:1400,Message:err.Error()}
		}
		if subirImagenResponse.Success {
			// Comparar Imagenes
			detectResponseBodys01,err := postDetect(personalFotoOriginal.UrlFoto)
			if err != nil {
				return model.UploadFileResponse{Status:false,ErrorCode:1500,Message:err.Error()}
			}
			detectResponseBodys02,err := postDetect(subirImagenResponse.URL)
			if err != nil {
				return model.UploadFileResponse{Status:false,ErrorCode:1600,Message:err.Error()}
			}
			var verifyRquestBody model.VerifyRequestBody
			for _, data := range detectResponseBodys01 {
				verifyRquestBody.FaceId1 = data.FaceId
			}
			for _, data := range detectResponseBodys02 {
				verifyRquestBody.FaceId2 = data.FaceId
			}
			verifyRespondBody, err := postVerify(verifyRquestBody)
			if err != nil {
				return model.UploadFileResponse{Status:false,ErrorCode:1700,Message:err.Error()}
			}
			if verifyRespondBody.IsIdentical {
				_, err = repository.AsistenciaInsert( model.Asistencia{
					CodEmpresa:     uploadFile.CodEmpresa,
					CodLocal:       uploadFile.CodLocal,
					TipDocIdent:    "01",
					NumDocIden:     uploadFile.DNI,
					TipoOperacion:  uploadFile.Status,
					Latitud:		0,
					Longitud:		0,
				})
				if err != nil {
					return model.UploadFileResponse{Status:false,ErrorCode:1800,Message:err.Error()}
				}

				respond := PostMarcacionRemote(MarcacionRemoteRequest{
					CodGrupoCia: "001",
					CodLocal:    uploadFile.CodLocal,
					Dni:         uploadFile.DNI,
				})
				if !respond.Status {
					log.Print(respond)
				}

				return model.UploadFileResponse{Status:verifyRespondBody.IsIdentical,ErrorCode:0,Message:""}
			}
			return model.UploadFileResponse{Status:false,ErrorCode:1900,Message:"ERROR : Usted no es la persona !!"}
		}

	default:
		return model.UploadFileResponse{Status:false,ErrorCode:2000,Message:fmt.Sprint("Status :(",uploadFile.Status,") no permitido")}
	}

	return model.UploadFileResponse{Status:false,ErrorCode:3000}
}

func loadAzureStorage(containerName string , filePhoto *multipart.FileHeader ) ( model.SubirImagenResponse,error ) {
	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)

	bodyWriter.WriteField("containerName", containerName )
	fileWriter, err := bodyWriter.CreateFormFile("file",filePhoto.Filename)
	if err != nil {
		return model.SubirImagenResponse{Success:false},err
	}

	filexx ,_ := filePhoto.Open()
	b, err := ioutil.ReadAll(filexx)
	fileWriter.Write(b)
	contentType := bodyWriter.FormDataContentType()
	bodyWriter.Close()

	resp, err := http.Post(config.CONFIG.AzureConfig.DataStorage.Url, contentType, bodyBuf)
	if err != nil {
		return model.SubirImagenResponse{Success:false},err
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		b, _ := ioutil.ReadAll(resp.Body)
		fmt.Errorf("[%d %s]%s", resp.StatusCode, resp.Status, string(b))
		return model.SubirImagenResponse{Success:false},fmt.Errorf("[%d %s]%s", resp.StatusCode, resp.Status, string(b))
	}
	respData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.SubirImagenResponse{Success:false},err
	}
	var subirImagenResponse model.SubirImagenResponse
	json.Unmarshal(respData, &subirImagenResponse)

	return subirImagenResponse,nil
}

func postDetect(urlFoto string) ([]model.DetectResponseBody, error ){
	var detectResponseBody []model.DetectResponseBody
	requestBody, err := json.Marshal(map[string]string{
		"url": urlFoto,
	})
	request, err := http.NewRequest("POST",config.CONFIG.AzureConfig.AzureCognitiveServices.Url.Detect,bytes.NewBuffer(requestBody))
	request.Header.Set("Content-Type","application/json")
	request.Header.Set("Ocp-Apim-Subscription-Key",config.CONFIG.AzureConfig.AzureCognitiveServices.Key)

	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return detectResponseBody,err
	}

	defer request.Body.Close()

	responseBody,err := ioutil.ReadAll(response.Body)
	if err != nil {
		return detectResponseBody,err
	}
	err = json.Unmarshal(responseBody, &detectResponseBody)
	if err != nil {
		err = errors.New(string(responseBody))
		log.Error(err)
		return detectResponseBody,err
	}
	return detectResponseBody,nil
}

func postVerify(body model.VerifyRequestBody) (model.VerifyResponseBody,error){
	var verifyResponseBody model.VerifyResponseBody
	bodyJson, _ := json.Marshal(body)
	request, err := http.NewRequest("POST",config.CONFIG.AzureConfig.AzureCognitiveServices.Url.Verify,bytes.NewBuffer(bodyJson))
	request.Header.Set("Content-Type","application/json")
	request.Header.Set("Ocp-Apim-Subscription-Key",config.CONFIG.AzureConfig.AzureCognitiveServices.Key)
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return verifyResponseBody,err
	}
	defer request.Body.Close()

	responseBody,err := ioutil.ReadAll(response.Body)
	if err != nil {
		return verifyResponseBody,err
	}
	json.Unmarshal(responseBody, &verifyResponseBody)
	return verifyResponseBody,nil
}
