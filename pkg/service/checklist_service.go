package service

import (
	"errors"
	"fps-app-ws-checklist-go/pkg/model"
	"fps-app-ws-checklist-go/pkg/repository"
	"log"
)

// ChecklistFindAll lista all
func ChecklistFindAll() []model.Checklist {
	return repository.ChecklistFindAll()
}

// GetChecklistByID lista x id
func GetChecklistByID(id int64) (model.Checklist, error) {
	checklist := repository.ChecklistFindByID(id)
	items, err := repository.ItemByChkList(id)
	if err != nil {
		return checklist,err
	}
	checklist.Items = items
	opcionesRespuestaList, _ := repository.OpcionRespuestaByChkListId(checklist.CodChList)
	for x, item := range checklist.Items {
		opcionesRespuestaListFilter := []model.OpcionRespuesta{}
		for _ , opcionRespuesta := range opcionesRespuestaList {
			if item.CodItem == opcionRespuesta.CodItem {
				opcionesRespuestaListFilter = append(opcionesRespuestaListFilter,opcionRespuesta)
			}
		}
		checklist.Items[x].OpcionesRespuesta = opcionesRespuestaListFilter
	}
	checklist.Users = repository.ChecklistUserByChkList(id)
	return checklist, nil
}

// ChecklistSave save checklist
func ChecklistSave(checklist model.Checklist) model.Checklist {
	checklist, err := repository.SaveCheckList(checklist)
	if err != nil {
		log.Println(err)
	}
	for _, item := range checklist.Items {
		for _,opcionesRespuesta := range item.OpcionesRespuesta {
			_,err := repository.SaveOpcionRespueata(opcionesRespuesta)
			if err != nil {
				log.Println(err)
			}
		}
		_, err := repository.SaveItem(item)
		if err != nil {
			log.Println(err)
		}
	}
	return checklist
}

func ChecklistUserList(estado int32, CodUsu string ) ( []model.ChecklistUserList , error ){
	checklistUserList , err := repository.ChecklistUserList(estado,CodUsu)
	if err != nil {
		return checklistUserList,err
	}
	return checklistUserList , nil
}

func GetChecklistListar(authorization string ) ([]model.ChecklistListarResponse,error) {
	checklistListarResponseList := []model.ChecklistListarResponse{}

	claims, Ok := ExtractClaims(authorization)
	if !Ok {
		return checklistListarResponseList , errors.New("Error obteniendo Codigo usuario")
	}
	CodUsuario := claims["cod_user"].(string)

	checklists,_ := repository.ChecklistFindByCodUser(CodUsuario)

	for _, checklist := range checklists {

		items, err := repository.ItemByChkList(checklist.CodChList)
		if err != nil {
			return checklistListarResponseList,nil
		}
		opcionesRespuestaList, _ := repository.OpcionRespuestaByChkListId(checklist.CodChList)
		itemListarResponseList := []model.ItemListarReponse{}
		for _ , item := range items {
			opcionRespuestaListarReponseList := []model.OpcionRespuestaListarReponse{}
			for _ , opcionRespuesta := range opcionesRespuestaList {
				if item.CodItem == opcionRespuesta.CodItem {
					opcionRespuestaListarReponseList = append(opcionRespuestaListarReponseList,
						model.OpcionRespuestaListarReponse{
							ID:          opcionRespuesta.ID,
							Enunciado:   *opcionRespuesta.Enunciado,
							Connotacion: opcionRespuesta.Connotacion,
						})
				}
			}
			itemListarResponseList = append(itemListarResponseList,
				model.ItemListarReponse{
					CodItem:              item.CodItem,
					Enunciado:            item.Enunciado,
					Obligatorio:          *item.Obligatorio,
					Criticidad:           item.Criticidad,
					RespuestaPositiva:    *item.RespuestaPositiva,
					PermiteComentarios:   *item.PermiteComentarios,
					Estado:               item.Estado,
					PermiteDecimales:     *item.PermiteDecimales,
					ValidoEnEstadisticas: *item.ValidoEnEstadisticas,
					TipoRespuesta:        *item.TipoRespuesta,
					PermiteImagen:        *item.PermiteImagen,
					Repeticion:			  false,
					OpcionRespuesta:	  opcionRespuestaListarReponseList,
				})
		}
		checklistListarResponseList = append(checklistListarResponseList,
			model.ChecklistListarResponse {
				Id: checklist.CodChList,
				TiempoVida: checklist.TiempoVida,
				TiempoVidaIni: 0,
				TiempoVidaFin: 0,
				RequiereLocal: checklist.RequiereLocal,
				RequiereQf: checklist.RequiereQF,
				RquiereAprobacionQF: checklist.RequiereAprobacionQF,
				CorreosNotificacion: *checklist.CorreosNotificacion,
				FechaCreacion: checklist.FechaCreacion.Unix(),
				UsuarioCreacion: *checklist.UsuarioCreacion,
				Estado: checklist.Estado,
				Titulo: checklist.Titulo,
				MsjBienvenida: *checklist.MsjBienvenida,
				MsjFin: *checklist.MsjFin,
				RequiereAvance: *checklist.GuardarAvance,
				RequiereT: checklist.RequiereT,
				RequiereAprobacionT: checklist.RequiereAprobacionT,
				RequiereAprobacionU: checklist.RequiereAprobacionU,
				Items:itemListarResponseList,
			})
	}
	return checklistListarResponseList,nil
}
