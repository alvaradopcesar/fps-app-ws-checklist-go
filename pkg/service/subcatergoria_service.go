package service

import (
	"fps-app-ws-checklist-go/pkg/model"
	"fps-app-ws-checklist-go/pkg/repository"
)

// SubCategoriaFindAll lista all
func SubCategoriaFindAll() []model.SubCategoria {
	return repository.SubCategoriaFindAll()
}
