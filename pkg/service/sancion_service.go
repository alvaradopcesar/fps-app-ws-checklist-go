package service

import (
	"fps-app-ws-checklist-go/pkg/model"
	"fps-app-ws-checklist-go/pkg/repository"
)

// SancionFindAll sancion all
func SancionFindAll() []model.Sancion {
	return repository.SancionFindAll()
}
