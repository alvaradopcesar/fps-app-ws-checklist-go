package service

import (
	"fps-app-ws-checklist-go/pkg/model"
	"fps-app-ws-checklist-go/pkg/repository"
)

// AreaFindAll lista all
func AreaFindAll() []model.Area {
	return repository.AreaFindAll()
}
