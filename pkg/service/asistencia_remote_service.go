package service

import (
	"bytes"
	"encoding/json"
	"fps-app-ws-checklist-go/config"
	"github.com/labstack/gommon/log"
	"io/ioutil"
	"net/http"
)

type MarcacionRemoteRequest struct {
	CodGrupoCia string `json:"cod_grupo_cia"`
	CodLocal    string `json:"cod_local"`
	Dni         string `json:"dni"`
}

type MarcacionRemoteResponse struct {
	Status  bool `json:"status"`
	Message string `json:"message"`
}

func PostMarcacionRemote(marcacionRemoteRequest MarcacionRemoteRequest) MarcacionRemoteResponse {
	requestBody, err := json.Marshal( marcacionRemoteRequest )
	if err != nil {
		return MarcacionRemoteResponse{Status:false,Message: err.Error()}
	}
	request, err := http.NewRequest("POST",config.CONFIG.Marcacion.Link,bytes.NewBuffer(requestBody))
	if err != nil {
		return MarcacionRemoteResponse{Status:false,Message: err.Error()}
	}
	request.Header.Set("Content-Type","application/json")

	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return MarcacionRemoteResponse{Status:false,Message: err.Error()}
	}

	defer request.Body.Close()

	responseBody,err := ioutil.ReadAll(response.Body)
	if err != nil {
		return MarcacionRemoteResponse{Status:false,Message: err.Error()}
	}

	marcacionRemoteResponse := MarcacionRemoteResponse{}
	err = json.Unmarshal(responseBody, &marcacionRemoteResponse)
	if err != nil {
		log.Error(err)
		return MarcacionRemoteResponse{Status:false,Message: err.Error()}
	}
	return marcacionRemoteResponse
}

