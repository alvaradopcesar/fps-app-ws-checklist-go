package config

import (
	"database/sql"
	"fps-app-ws-checklist-go/util"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/gorp.v1"
)

// Dbmap ar to conect oracle
var Dbmap = InitDb()

// InitDb uncion que carga la conecccion a al BD Global
func InitDb() *gorp.DbMap {

	cadenaCon := CONFIG.DbConfig.User + ":" + CONFIG.DbConfig.Password + "@tcp(" + CONFIG.DbConfig.Server + ":" + CONFIG.DbConfig.Port + ")/" + CONFIG.DbConfig.Database + CONFIG.DbConfig.Parameters
	log.Println("Config found, name = ", cadenaCon)

	db, err := sql.Open("mysql", cadenaCon)
	util.CheckErr(err, "Error coneccion en mysql "+cadenaCon)

	if err := db.Ping(); err != nil {
		util.CheckErr(err, "Error coneccion en mysql "+cadenaCon)
		db.Close()
	}
	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.MySQLDialect{}}

	return dbmap
}
