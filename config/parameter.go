package config

import (
	"github.com/spf13/viper"
	"log"
	"os"
)

var CONFIG = UploadConfig()

type Config struct {
	DbConfig struct {
		Server 		string `mapstructure:"server"`
		Port 		string `mapstructure:"port"`
		User 		string `mapstructure:"user"`
		Password 	string `mapstructure:"password"`
		Database 	string `mapstructure:"database"`
		Parameters 	string `mapstructure:"parameters"`
	} `mapstructure:"db"`
	AzureConfig struct {
		DataStorage struct {
			Url       string `mapstructure:"url"`
			Container struct {
				ContainerChecklist string `mapstructure:"checklist"`
				ContainerTmp       string `mapstructure:"tmp"`
			} `mapstructure:"container"`
		} `mapstructure:"datastorage"`
		AzureCognitiveServices struct{
			Key string `mapstructure:"key"`
			Url struct {
				Detect string `mapstructure:"detect"`
				Verify string `mapstructure:"verify"`
			} `mapstructure:"url"`
		} `mapstructure:"cognitive_services"`
	} `mapstructure:"azure"`
	FirebaseConfig struct{
		ServerKey	string `mapstructure:"server_key"`
		ApiUrl 		string `mapstructure:"api_url"`
	} `mapstructure:"firebase"`
	Marcacion struct{
		Link 	string `mapstructure:"link"`
	} `mapstructure:"marcacion"`
}

func UploadConfig() *Config {
	var config Config

	viper.SetConfigType("yml")
	nameFileProperties := "properties"
	env, found := os.LookupEnv("PROFILE")
	if found  {
		nameFileProperties = nameFileProperties + "-"+ env
	}
	viper.SetConfigName(nameFileProperties) // name of config file (without extension) envoca un archivo json y actualiza la informacion
	viper.AddConfigPath(".")          // path to look for the config file in
	err := viper.ReadInConfig()
	if err != nil {
		log.Println(err)
		panic(err)
	}
	err = viper.Unmarshal(&config)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}

	log.Println("Properties found, name = ", nameFileProperties)
	log.Println(config)
	return &config
}

