module fps-app-ws-checklist-go

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/denisenkom/go-mssqldb v0.0.0-20190830225923-3302f0226fbd // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.4.1
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0
	github.com/magiconair/properties v1.8.0
	github.com/pkg/errors v0.8.0
	github.com/spf13/viper v1.4.0
	gopkg.in/gorp.v1 v1.7.2
)

go 1.13
