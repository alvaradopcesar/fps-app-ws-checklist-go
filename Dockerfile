FROM golang:1.13.1-alpine3.10
RUN apk add git gcc g++ libc-dev
WORKDIR /go/src/fps-app-ws-checklist-go/

COPY . .

ENV GO111MODULE=on
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo cmd/checklist/main.go

FROM alpine:latest
RUN apk --no-cache add ca-certificates tzdata
ENV TZ America/Lima
ENV PROFILE=LOCAL
RUN mkdir template_excel
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
COPY --from=0 /go/src/fps-app-ws-checklist-go/main .
COPY --from=0 /go/src/fps-app-ws-checklist-go/properties.yml .
COPY --from=0 /go/src/fps-app-ws-checklist-go/properties-local.yml .
COPY --from=0 /go/src/fps-app-ws-checklist-go/properties-dev.yml .
COPY --from=0 /go/src/fps-app-ws-checklist-go/properties-qa.yml .
COPY --from=0 /go/src/fps-app-ws-checklist-go/properties-prd.yml .
COPY --from=0 /go/src/fps-app-ws-checklist-go/template_excel/*.* template_excel/
EXPOSE 8082
ENTRYPOINT ["/main"]  
