// Copyright 2017 Emir Ribic. All rights reserved.

// Golang SwaggerUI example Cesar
//
// This documentation describes APIs found
//
//     Schemes: https
//     BasePath: /v1
//     Version: 1.0.0
//     License: MIT http://opensource.org/licenses/MIT
//     Host: ribice.ba/goswagg
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Security:
//     - bearer
//
//     SecurityDefinitions:
//     bearer:
//          type: apiKey
//          name: Authorization
//          in: header
//
// swagger:meta

package main

import (
	"fps-app-ws-checklist-go/pkg/controller"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {

	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.CORS())

	e.POST("/login", controller.LoginWeb)
	e.POST("/loginMobile", controller.LoginMobile)

	h := e.Group("/v1")
	h.Use(middleware.JWT([]byte("secret")))

	// swagger:operation GET /usuario repos repoList
	// ---
	// summary: List the users .
	// description: If author length is between 6 and 8, Error Not Found (404) will be returned.
	// responses:
	//   "200":
	//     "$ref": "#/responses/reposResp"
	//   "400":
	//     "$ref": "#/responses/badReq"
	//   "404":
	//     "$ref": "#/responses/notFound"
	// h.GET("/usuario/:id", rest.GetUserByID)
	// h.PUT("/usuario/:id", rest.PutUserByID)

	h.GET("/obtenerMaestros", controller.GetMaestros)
	h.GET("/checklist", controller.GetChecklist)
	h.POST("/checklist", controller.PostChecklist)
	h.GET("/checklist/:id", controller.GetChecklistByID)
	h.GET("/listar", controller.GetChecklistListar)
	h.GET("/niveles", controller.GetNivel)
	h.GET("/arbol",controller.GetArbol)
	h.GET("/niveles/userlocal/:nivelID",controller.GetNivelUsuarioLocaByNivelID)

	h.POST("/respuestaChecklist",controller.PostRespuestaChecklist)

	h.POST("/responder",controller.PostRespuestaChecklistMovil)

	h.GET("/sancion", controller.GetSancion)
	h.GET("/area", controller.GetArea)
	h.GET("/categoria", controller.GetCategoria)
	h.GET("/subcategoria", controller.GetSubCategoria)

	h.POST("/nivel",controller.PostNivel)

	h.GET("/cheklistuser/:estado/:codUsu",controller.GetChecklistUserList)
	h.GET("/usuario/local/:nombreToFind", controller.GetUsuarioLocalList )

	// Face Recognition

	h.POST("/validaPhoto",controller.PostExistePhoto)
	h.POST("/validaDNI",controller.PostExisteDNI)
	h.POST("/uploadPhoto",controller.PostUploadFile)
	h.GET("/listaAsistencia/:DNI",controller.GetListaAsistencia)

	h.POST("/marcacion",controller.PostAsistenciaMarcacion)

	// Geolocalizacion
	h.GET("/LocalByID/dni/:DNI/lati/:latitud/long/:longitud",controller.GetLocalByID)

	// Tiendas por Usuario
	h.GET("/localByUser",controller.GetLocalesByDNI)

	// Export Files
	h.GET("/exportFormat01/fecIni/:fecini/fecFin/:fecfin",controller.GetChecklistExportTemplate01)
	h.GET("/exportFormat02/fecIni/:fecini/fecFin/:fecfin",controller.GetAsistenciaByFechas)

	e.Start(":8082")

}
